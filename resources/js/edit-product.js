import Dropzone from "dropzone";

let myDropzone = new Dropzone(".dropzone",{
    acceptedFiles: "image/png,image/jpeg,",
    previewsContainer: "#dropzone-previews",
    autoQueue: true,
    autoProcessQueue: true,
    uploadMultiple: true,
    url:product_photo_store_route,
    parallelUploads: max_photos,
    maxFiles: max_photos,
    thumbnailWidth:null,
    thumbnailHeight:200,
    previewTemplate: document.querySelector('#dbz-template').outerHTML,
    init: function() {
        //FOR MORE INFORMATION ON LIST OF AVAILABLE EVENTS CHECK THIS LINK(https://github.com/dropzone/dropzone/discussions/2041)
        this.on("addedfile", function(file) {
            console.log('File added:', file);
            $(file.previewElement).find(".btn-dz-delete").off('click');
            $(file.previewElement).find(".btn-dz-delete").on('click',function(){
                if($(file.previewElement).find('.dz-label .product-photo').val() > 0){

                    $.ajax({
                        url:product_photo_delete_route.replace("replace",$(file.previewElement).find('.dz-label .product-photo').val()),
                        data:{product_photo:$(file.previewElement).find('.dz-label .product-photo').val(),_token:$("input[name=_token]").val()},
                        type:"DELETE",
                        success: function(result) {
                            console.log(result);
                        }
                    })
                }
            });

            $(file.previewElement).find(".dz-image-container").css('background-image', 'url('+file.url+')');
            if (typeof file.id !== 'undefined') {
                var chk_id = "cover_" + file.id;
                var photo_id = file.id;

                if($(".main_image_id").val() == photo_id){
                    $(".summary_photo").css('background-image', 'url('+file.url+')');
                }
                $(file.previewElement).find(".form-check-input").off('change');
                $(file.previewElement).find(".form-check-input").on('change', function () {
                    $(".main_image_id").val(photo_id);
                    $(".summary_photo").css('background-image', 'url('+file.url+')');
                });
                $(file.previewElement).find(".product-cover").show();
                $(file.previewElement).find(".form-check-input").attr('id', chk_id);
                $(file.previewElement).find(".form-check-label").attr('for', chk_id);
            }
        });
        this.on("sendingmultiple", function() {
            // console.log("sendingmultiple");
        });
        this.on("sending", function(file, xhr, formData) {
            // console.log("sending");
            formData.append("_token", $("input[name=_token]").val());
            $('.dropzone input[name="product_photo[]"].success-photo').each(function (index, photo) {
                formData.append("product_photo[]", $(photo).val());
            });
        });
        this.on("successmultiple", function(files, response) {
            console.log("successmultiple",[files,response]);
            if (response.success) {
                for (const [key, file] of Object.entries(files)) {
                    console.log(key, file);
                    var photo_id = response.data[key];
                    $(file.previewElement).find(".dz-image-container").css('background-image', 'url('+file.dataURL+')');
                    $(file.previewElement).find('.dz-label .product-photo').addClass("success-photo");
                    $(file.previewElement).find('.dz-label .product-photo').val(photo_id);

                    var chk_id = "cover_" + photo_id;
                    $(file.previewElement).find(".form-check-input").off('change');
                    $(file.previewElement).find(".form-check-input").on('change', function () {
                        var xid = $(file.previewElement).find('.dz-label .product-photo').val();
                        $(".main_image_id").val(xid);
                        $(".summary_photo").css('background-image', 'url('+file.dataURL+')');
                    });
                    $(file.previewElement).find(".product-cover").show();
                    $(file.previewElement).find(".form-check-input").attr('id', chk_id);
                    $(file.previewElement).find(".form-check-label").attr('for', chk_id);
                }
            }
        });
        this.on("success", function(file,response) {
            // console.log("success",[file,response]);
            // $(file.previewElement).find('.btn-dz-retry').hide();
        });
        this.on("removedfile", function(file, response) {
            console.log("removedfile",[response,file]);
        });
        this.on("error", function(file, response) {
            console.log("error",[file,response]);
            $(file.previewElement).find('.dz-label').removeClass("col-md-10").addClass("col-md-8");
            $(file.previewElement).find('.dz-buttons').removeClass("col-md-2").addClass("col-md-4");
            $(file.previewElement).find('.btn-dz-retry').show();

            $(file.previewElement).find(".btn-dz-retry").off('click');
            $(file.previewElement).find(".btn-dz-retry").on('click',function(){
                $(file.previewElement).find('.btn-dz-retry').hide();
                $(file.previewElement).find('.dz-label').removeClass("col-md-8").addClass("col-md-10");
                $(file.previewElement).find('.dz-buttons').removeClass("col-md-4").addClass("col-md-2");
                file.status = Dropzone.QUEUED
                file.upload.progress = 0;
                file.upload.bytesSent = 0;
                myDropzone.processQueue();
            });
            if(file.status === "error" && file.accepted === false){
                $(file.previewElement).find('.dz-label .dz-error-message > span').html(response);
            }
        });
        this.on("errormultiple", function(files, response) {
            // console.log("errormultiple",response);
            $(".dz-error-message").find("span").html("");
            dbzUpload.innerHTML = "Save Changes";
            dbzUpload.removeAttribute('disabled');
            document.querySelector("#createProductModal .errorMessage").style.display = "unset";
            document.querySelector("#createProductModal .errorMessage").innerHTML = response.message;

            if(response.errors){
                $("#product-dropzone .alert").remove();
                for (const [key, value] of Object.entries(response.errors)) {
                    // console.log(`${key}: ${value}`);
                    if($("#"+key).parent().find(".alert").length == 0){
                        $("#"+key).parent().prepend('<div class="alert alert-danger">'+value+'</div>');
                    }
                }
                createProductModal.hide();
                $("alert")[0].scrollIntoView();
            }
        });
    }
});

let dbzUpload = document.querySelector('.dbz-upload');
const createProductModal = new bootstrap.Modal(document.getElementById('createProductModal'));
const btnSaveChanges = document.querySelector(".btnSaveChanges");
const productForm = document.querySelector("#productForm");

dbzUpload.addEventListener("click",function(e){
    console.log("SUBMIT");
    document.querySelector("#createProductModal .errorMessage").style.display = "none";
    document.querySelector("#createProductModal .errorMessage").innerHTML = "";
    if( $("#productForm")[0].checkValidity() ) {
        e.preventDefault();
        e.stopPropagation();
        if( myDropzone.getAcceptedFiles().length > 0 || product_photos.length > 0 ) {
            dbzUpload.innerHTML = "Saving...";
            dbzUpload.setAttribute('disabled', 'disabled');
            $("#productForm").submit();
        } else {
            document.querySelector("#createProductModal .errorMessage").style.display = "unset";
            document.querySelector("#createProductModal .errorMessage").innerHTML = "Please add images first.";
        }
    } else {
        createProductModal.hide();
        $("#productForm")[0].reportValidity();
        return false;
    }
});

btnSaveChanges.addEventListener("click", () => {
    createProductModal.show();
});

$(function () {
    for (let i = 0; i < product_photos.length; i++) {
        myDropzone.displayExistingFile(product_photos[i], product_photos[i]['url']);
        $($("#dropzone-previews .dz-label .product-label")[i]).val(product_photos[i]['description']);
        $($("#dropzone-previews .dz-label .product-photo")[i]).val(product_photos[i]['id']);
        if($(".main_image_id").val() == product_photos[i]['id']){
            $($("#dropzone-previews .dz-details .product_cover")[i]).prop("checked", true);
        }
        $($("#dropzone-previews .dz-details .product-cover")[i]).show();
        $($("#dropzone-previews .dz-label .product-photo")[i]).addClass('success-photo');
    }

    $(window).on('resize', function (e) {
        if ($(window).width() >= 768) {

        } else {
            $('.fixedElement').css({ 'position': 'static', 'top': '0', 'width': $('.fixedElement').parent().css('width') });
        }
    });
    $(window).scroll(function (e) {
        if ($(window).width() >= 768) {
            checkPosition(window);
        } else {
            $('.fixedElement').css({ 'position': 'static', 'top': '0', 'width': $('.fixedElement').parent().css('width') });
        }
    });

    if ($(window).width() >= 768 && ($(window).scrollTop() < 200 && !($('.fixedElement').css('position') === 'fixed'))) {
        $('.fixedElement').css({ 'position': 'static', 'top': '0', 'width': $('.fixedElement').parent().css('width') });
    }

    if (($("#part1").find(".alert.alert-danger").length > 0) || (product_photos.length > 0 && $("#part2").find(".alert.alert-danger").length > 0) || ($("#part3").find(".alert.alert-danger").length > 0)) {
        $("#part1 .accordion-button").removeClass("collapsed");
        $("#part1 .accordion-collapse").addClass("show");
        $("#part2 .accordion-button").removeClass("collapsed");
        $("#part2 .accordion-collapse").addClass("show");
        $("#part3 .accordion-button").removeClass("collapsed");
        $("#part3 .accordion-collapse").addClass("show");
        $(".alert")[0].scrollIntoView();
    }
});

function checkPosition(x) {
    var $el = $('.fixedElement');
    var isPositionFixed = ($el.css('position') == 'fixed');
    $(".main-panel").css('min-height', $(".fixedElement").height());

    if (isPositionFixed && ((document.body.offsetHeight - $(".template-footer").height()) < (window.scrollY + $el.height()))) {
        $el.css({ 'position': 'fixed', 'top': 'unset', 'bottom': $(".template-footer").height(), 'width': $el.parent().css('width') });
    } else if (isPositionFixed && (document.body.offsetHeight - $(".template-footer").height()) > (window.scrollY + $el.height())) {
        if ($(x).scrollTop() > 250 && !isPositionFixed) {
            $el.css({ 'position': 'fixed', 'top': '100px', 'bottom': 'unset', 'width': $el.parent().css('width') });
        }
        if ($(x).scrollTop() < 150 && isPositionFixed) {
            $el.css({ 'position': 'static', 'top': '0', 'bottom': 'unset', 'width': $el.parent().css('width') });
        }
    } else {
        if ($(x).scrollTop() > 250 && !isPositionFixed) {
            $el.css({ 'position': 'fixed', 'top': '100px', 'bottom': 'unset', 'width': $el.parent().css('width') });
        }
        if ($(x).scrollTop() < 150 && isPositionFixed) {
            $el.css({ 'position': 'static', 'top': '0', 'bottom': 'unset', 'width': $el.parent().css('width') });
        }
    }
}
let isRental = false;
let isUsed;
$("#rental").on('click', function () {
    isRental = $(this).is(":checked");
    $("#rental_fields").attr("hidden", !isRental);
    if (isRental) {
        $("#rental_price_daily").attr("required", isRental);
        $("#rental_price_weekly").attr("required", isRental);
        $("#rental_price_monthly").attr("required", isRental);
        $("#rental_price_daily").val(0);
        $("#rental_price_weekly").val(0);
        $("#rental_price_monthly").val(0);
    }

});
$("[name='product_condition_id']").on('change', function () {
    isUsed = $(this).val() === '2';
    $("[name='hours_of_use']").attr("required", isUsed);
});
$("[name='year']").on('change', function () {
    $("#summary_year").html($(this).val());
});
$("[name='price']").on('keyup', function () {
    $("#summary_price").html($(this).val() + " PHP");
});
$("[name='model']").on('keyup', function () {
    $("#summary_model").html($(this).val());
});
$("[name='description']").on('keyup', function () {
    $("#summary_description").html($(this).val());
});
$("[name='title']").on('keyup', function () {
    $("#summary_product_title").html($(this).val());
});
$("[name='contact_address']").on('keyup', function () {
    $("#summary_location").html($(this).val());
});
$("[name='category']").on('change', function () {
    var selectedCategory = $(this).val();
    $.ajax({
        type: "GET",
        url: '../get-subcategories/' + selectedCategory, // This is what I have updated
    }).done(function( data ) {
        $("[name='subcategory']").empty();
        $.each(data, function (index, item) {
            $("[name='subcategory']").append($('<option>', {
                value: item.id,
                text : item.name
            }));
        });
    });
});
