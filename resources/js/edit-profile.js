require('exif-js');

var uploadCrop;
var uCrop = document.getElementById('upload-croppie');

var croppieModal = document.getElementById('croppieModal');
var modal = bootstrap.Modal.getOrCreateInstance(croppieModal) // Returns a Bootstrap modal instance

function demoUpload() {

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                // $('.upload-demo').addClass('ready');
                $(".upload-croppie-container").show();
                uploadCrop.bind({
                    url: e.target.result
                }).then(function(){
                    console.log('jQuery bind complete');
                });

            }

            reader.readAsDataURL(input.files[0]);
        }
        else {
            alert("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    uploadCrop = new Croppie(uCrop, {
        viewport: {
            width: 300,
            height: 300,
            type: 'circle'
        },
        boundary: {
            width: 300,
            height: 300
        },
        enableExif: true
    });
    $('#profile_photo_upload').on('change', function () { readFile(this); });
    $('.upload-result').on('click', function (ev) {
        uploadCrop.result({
            type: 'canvas',
            size: 'viewport',
        }).then(function (resp) {
            $(".profile_picture_img").attr('src',resp);
            $("input[name=profile_photo]").val(resp);
            modal.hide();
        });
    });

}
$(function(){
    demoUpload();
});
