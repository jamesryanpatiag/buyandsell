require('./bootstrap');
require('alpinejs');

$(function(){
    const userPanelToggle = document.querySelector(".user-panel-toggle");
    const btnLogout = document.querySelector(".btnLogout");
    const userPanel = document.querySelector(".user-panel");
    const formLogout = document.querySelector("form[name=formLogout]");

    // const hamburger = document.querySelector(".hamburger");
    // const navLinks = document.querySelector(".nav-links");
    // const navLink = document.querySelector(".nav-link");
    // const navItem = document.querySelector(".nav-item");

    if(formLogout !== null){
        const logoutModal = new bootstrap.Modal(document.querySelector("#logoutModal"));
        btnLogout.addEventListener("click", () => {
            logoutModal.show();
        });
    }

    if(userPanelToggle !== null) {
        userPanelToggle.addEventListener("click", () => {
            userPanel.classList.toggle("open");
        });
    }


    // hamburger.addEventListener("click", () => {
    //     hamburger.classList.toggle("open");
    //     navLinks.classList.toggle("open");
    // });
});
