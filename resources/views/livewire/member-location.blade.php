@push('base_class')
    @if(in_array($currentRoute,array('product.create','profile-edit')))
        col-md-4
    @endif
@endpush
<div class="row @if($currentRoute == 'profile.edit') mb-5 @endif">
    <x-form.select name="{{$prefix}}location_area_id" :options="$areas" wire:model="area" placeholder="Area"/>
    @if(count($regions) > 0)
        <x-form.select name="{{$prefix}}location_region_id" :options="$regions" wire:model="region" placeholder="Region"/>
    @endif
    @if(count($provinces) > 0)
        <x-form.select name="{{$prefix}}location_province_id" :options="$provinces" wire:model="province" placeholder="Province"/>
    @endif
</div>
