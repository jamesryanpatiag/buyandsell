<div>
    <x-form.input name="email" :error-message="$errorMessage" :success-message="$successMessage" wire:model="email" placeholder="Email"/>
</div>
