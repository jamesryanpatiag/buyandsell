<div>
    <x-form.input name="username" :error-message="$errorMessage" :success-message="$successMessage" wire:model="username" placeholder="Username"/>
</div>
