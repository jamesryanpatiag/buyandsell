<footer class="template-footer w-100 mt-auto">
    <div class="container py-5">
        <div class="row">
            <div class="col">
                <hr class="mb-4">
            </div>
        </div>
        <div class="row">
            <div class="col-12 order-2 col-lg-4 order-lg-0 col-xl-3 my-3 my-lg-0 justify-content-center justify-content-xl-start align-items-center d-flex">
                <img src="{{ asset('img/brand-logo.png') }}" width="38px" height="37.75px"  class="ms-1 me-3" />
                <span>&copy; Equipment Exchange</span>
            </div>
            <div class="col-12 order-0 col-lg-5 order-lg-1 col-xl-7 col-xxl-6 my-3 my-lg-0 justify-content-center justify-content-lg-start align-items-center d-flex">
                <ul class="nav justify-content-center justify-content-lg-start">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="#">Help Center</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Jobs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Advertise with Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Terms</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Privacy</a>
                    </li>
                </ul>
            </div>
            <div class="col-12 order-1 col-lg-3 order-lg-2 col-xl-2 col-xxl-3 my-3 my-lg-0 justify-content-center footer-soc-icons align-items-center d-flex">
                <a target="_blank" href="{{ config('site.social.snapchat') }}" class="mx-2"><i class="soc-icons fab fa-snapchat-square"></i></a>
                <a target="_blank" href="{{ config('site.social.twitter') }}" class="mx-2"><i class="soc-icons fab fa-twitter"></i></a>
                <a target="_blank" href="{{ config('site.social.facebook') }}" class="mx-2"><i class="soc-icons fab fa-facebook-square"></i></a>
                <a target="_blank" href="{{ config('site.social.instagram') }}" class="mx-2"><i class="soc-icons fab fa-instagram"></i></a>
            </div>
        </div>
    </div>
</footer>
