<x-app-layout>
    <div class="container">
        @yield('content_body')
    </div>
</x-app-layout>
