<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <x-form.input :value="$product ?? $profile->first_name ?? null" placeholder="First Name" name="contact_first_name" />
        </div>
        <div class="col-12">
            <x-form.input :value="$product ?? $profile->last_name ?? null" placeholder="Last Name" name="contact_last_name" />
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <x-form.input :value="$product ?? null" placeholder="Address" name="contact_address" />
        </div>
        <div class="col-12">
            <livewire:member-location :value="$product->contact_location_province_id ?? $profile->location_province_id ?? null" prefix="contact_"/>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="row">
                <x-form.input placeholder="Contact Number" name="contact_number" :value="$product ?? $profile->mobile_number ?? null" />
            </div>
        </div>
    </div>
</div>
