<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <input type="hidden" class="main_image_id" name="main_image_id" @if(@$product) value="{{ trim($product->main_image_id) }}" @else value="{{ trim(old('main_image_id')) }}" @endif/>
            <div class="dropzone">
                <div id="dropzone-previews" class="row">
                    <div class="dz-default dz-message col-12"><button class="dz-button" type="button">Drop files here to upload</button></div>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-12">
                    <x-form.input :value="$product ?? null" name="youtube_url" :required="false" placeholder="Youtube Link" />
                </div>
            </div>
        </div>
    </div>
</div>
