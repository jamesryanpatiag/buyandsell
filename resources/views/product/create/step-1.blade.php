<div class="container-fluid">
    <div class="row">
        <form class="settings-form" action="#" method="post">
            @csrf
            <div class="row">
                <div class="row">
                    <div class="col">
                        <x-form.input :value="$product ?? null" name="title" placeholder="Product Title" required />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.textarea :value="$product ?? null" name="description" placeholder="Description" required />
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col col-md-10 col-lg-8 col-xl-4 d-flex align-items-center">
                        <x-form.input :value="$product ?? 0" name="price" placeholder="Price" required suffix="PHP" class="text-end" />
                    </div>

                </div>
                <div class="row">
                        <div class="row">
                            <div class="col col-md-6">
                                <div class="form-check form-switch">
                                    <input class="form-check-input" name="rental" type="checkbox" onclick="toggleRental()" id="rental" value="1" checked>
                                    <label class="form-check-label" for="rental">Rental</label>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="rental_fields">
                            <div class="col col-md-4">
                                <x-form.input :value="$product ?? 0" type="number" name="rental_price_daily" id="rental_price_daily" placeholder="Daily Price" required />
                            </div>
                            <div class="col col-md-4">
                                <x-form.input :value="$product ?? 0" type="number" name="rental_price_weekly" id="rental_price_weekly" placeholder="Weekly Price" required />
                            </div>
                            <div class="col-md-4">
                                <x-form.input :value="$product ?? 0" type="number" name="rental_price_monthly" placeholder="Monthly Price" required />
                            </div>
                        </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <x-form.input :value="$product ?? null" name="model" placeholder="Model" required />
                    </div>
                    <div class="col-12">
                        <x-form.select :value="$product ?? null" id="category" name="category" placeholder="Category" :options="\App\Models\Category::selectOptions()" />
                    </div>
                    <div class="col-12">
                        <x-form.select :value="$product ?? null" name="subcategory" placeholder="Subcategory" :options="[]" />
                    </div>
                    <div class="col-12">
                        <x-form.select :value="$product ?? null" name="brand" placeholder="Brand" :options="\App\Models\Brand::selectOptions()" />
                    </div>
                    <div class="col-12">
                        <x-form.select :value="$product ?? null" name="product_condition_id" placeholder="Condition" :options="\App\Models\ProductCondition::selectOptions()" />
                    </div>
                    <div class="col-12">
                        <x-form.select :value="$product ?? null" name="product_status_id" placeholder="Status of the Product" :options="\App\Models\ProductStatus::selectOptions()" />
                    </div>
                    <div class="col-12">
                        <x-form.select :value="$product ?? null" name="year" placeholder="Year" :options="\App\Models\Product::getYearRange()" />
                    </div>
                    <div class="col-12">
                        <x-form.select :value="$product ?? null" name="equipment_mileage_id" placeholder="Equipment Mileage" :options="\App\Models\EquipmentMileage::selectOptions()" />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <h5>Additional Information</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <x-form.input :value="$product ?? null" name="serial_number" :required="false" placeholder="Serial Number"  />
                    </div>
                    <div class="col-12">
                        <x-form.input :value="$product ?? null" name="plate_number" :required="false" placeholder="Plate Number"  />
                    </div>
                    <div class="col-12">
                        <x-form.input :value="$product ?? null" name="engine_make" :required="false" placeholder="Engine Make"  />
                    </div>
                    <div class="col-12">
                        <x-form.input :value="$product ?? null" name="hours_of_use" :required="false" placeholder="Hours of Use (hr)"  />
                    </div>
                    <div class="col-12">
                        <x-form.select :value="$product ?? null" name="fuel_type_id" :required="false" placeholder="Fuel Type" :options="\App\Models\FuelType::selectOptions()"  />
                    </div>
                    <div class="col-12">
                        <x-form.input :value="$product ?? null" name="front_tire_size" :required="false" placeholder="Front Tire Size"  />
                    </div>
                    <div class="col-12">
                        <x-form.input :value="$product ?? null" name="rear_tire_size" :required="false" placeholder="Rear Tire Size"  />
                    </div>
                    <div class="col-12">
                        <x-form.input :value="$product ?? null" name="max_height" :required="false" placeholder="Max Height (In)"  />
                    </div>
                    <div class="col-12">
                        <x-form.input :value="$product ?? null" name="max_width" :required="false" placeholder="Max Width (In)"  />
                    </div>
                    <div class="col-12">
                        <x-form.input :value="$product ?? null" name="weight" :required="false" placeholder="Weight (lbs)"  />
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
