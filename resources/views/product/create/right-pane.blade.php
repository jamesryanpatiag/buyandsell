<div class="d-flex">
    <div class="fixedElement" style="position: static; top: 0;width:100%;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h4>Product Listing View</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="border border-dark text-xs">
                        <div class="mx-3 mt-3">
                            <div class="col text-end"><span id="summary_price"></span></div>
                        </div>
                        <div class="mx-3 mb-3">
                            <div class="col">
                                <span id="summary_year"></span>
                            </div>
                            <div class="col">
                                <span id="summary_product_title"></span>
                            </div>
                            <div class="col">
                                <span id="summary_model"></span>
                            </div>
                        </div>
                        <div class="px-0 right-pane-container border border-dark mx-3 summary_photo" @if(@$product)style="background-image:url('{{ $product->main_image()->getFullUrl() }}');" @endif>
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAW0AAAFtCAQAAABmCPOpAAAC9ElEQVR42u3SMQ0AAAzDsJU/6aHoU9kQouRgUiTA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZUPEkrAW5vc3dnAAAAAElFTkSuQmCC" class="img-fluid">
                        </div>
                        <div class="px-3 mt-3">
                            <span id="summary_description"></span>
                        </div>
                        <div class="px-3 mb-3">
                            <span id="summary_location"></span>
                        </div>
                    </div>
                    <div class="text-xs fw-bold">
                        <div class="right-pane-divider ">
                            CHOSEN SUBSCRIPTION PACKAGE
                        </div>
                        <div class="right-pane-divider">
                            <div class="col-md-4">
                                <div>{{ @$package->duration }}</div>
                                <div>{{ @$package->photo_limit }} @if(@$package->photo_limit > 1) PHOTOS
                                    @else VIDEO
                                    @endif</div>
                                <div>{{ @$package->can_add_video }} @if(@$package->can_add_video > 1) VIDEOS
                                    @else VIDEO
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="right-pane-divider row">
                            <div class="text-start col">TOTAL</div>
                            <div class="text-end col" name="summary_total">0.00 PHP</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col mt-3">
                    <button type="button" class="btn btnSaveChanges btn-sm btn-outline-secondary">Save Changes</button>
                </div>
            </div>
        </div>
    </div>
</div>
