<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-md-6">
            <x-form.input placeholder="First Name" name="first_name" :value="$product" />
        </div>
        <div class="col-12 col-md-6">
            <x-form.input placeholder="Last Name" name="last_name" :value="$product" />
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <x-form.input placeholder="Address" name="address" :value="$product" />
        </div>
        <div class="col-12">
            <livewire:member-location />
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="row">
                <x-form.input placeholder="Contact Number" name="contact_number" :value="$product" />
            </div>
        </div>
    </div>
</div>
