<div class="container-fluid">
    <div class="row">
        <form class="settings-form" action="#" method="post">
            @csrf
            <div class="row">
                <div class="row">
                    <div class="col">
                        <x-form.input name="title" placeholder="Product Title" required :value="$product" />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.textarea name="description" placeholder="Description" required :value="$product" />
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col col-md-6 d-flex align-items-center">
                        <x-form.input name="price" placeholder="Price (PHP)" required :value="$product" />
                    </div>

                </div>
                <div class="row">
                    <livewire:products />
                </div>
                <div class="row">
                    <div class="col col-md-6">
                        <x-form.input name="model" placeholder="Model" required :value="$product" />
                    </div>
                    <div class="col col-md-6">
                        <x-form.select name="brand_id" placeholder="Brand" :options="\App\Models\Brand::selectOptions()" :value="$product" />
                    </div>
                    <div class="col col-md-6">
                        <x-form.select name="industry_category_id" placeholder="Industries" :options="\App\Models\IndustryCategory::selectOptions($product->industry_category_id)" :required="false" />
                    </div>
                    <div class="col col-md-6">
                        <x-form.select name="product_class_id" placeholder="Class" :options="\App\Models\ProductClass::selectOptions()" :value="$product" />
                    </div>
                    <div class="col col-md-6">
                        <x-form.select name="product_condition_id" placeholder="Condition" :options="\App\Models\ProductCondition::selectOptions()" :value="$product" />
                    </div>
                    <div class="col col-md-6">
                        <x-form.select name="product_status_id" placeholder="Status of the Product" :options="\App\Models\ProductStatus::selectOptions()" :value="$product" />
                    </div>
                    <div class="col col-md-6">
                        <x-form.select name="year" placeholder="Year" :options="\App\Models\Product::getYearRange()" :value="$product" />
                    </div>
                    <div class="col col-md-6">
                        <x-form.select name="equipment_mileage_id" placeholder="Equipment Mileage" :options="\App\Models\EquipmentMileage::selectOptions()" :value="$product" />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <h5>Additional Information</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-6">
                        <x-form.input name="serial_number" placeholder="Serial Number" :required="false" :value="$product" />
                    </div>
                    <div class="col col-md-6">
                        <x-form.input name="plate_number" placeholder="Plate Number" :required="false" :value="$product" />
                    </div>
                    <div class="col col-md-6">
                        <x-form.input name="engine_make" placeholder="Engine Make" :required="false" :value="$product" />
                    </div>
                    <div class="col col-md-6">
                        <x-form.input name="hours_of_use" placeholder="Hours of Use (hr)" :required="false" :value="$product" />
                    </div>
                    <div class="col col-md-6">
                        <x-form.select name="fuel_type_id" placeholder="Fuel Type" :options="\App\Models\FuelType::selectOptions()" :required="false" :value="$product" />
                    </div>
                    <div class="col col-md-6">
                        <x-form.input name="front_tire_size" placeholder="Front Tire Size" :required="false" :value="$product" />
                    </div>
                    <div class="col col-md-6">
                        <x-form.input name="rear_tire_size" placeholder="Rear Tire Size" :required="false" :value="$product" />
                    </div>
                    <div class="col col-md-6">
                        <x-form.input name="max_height" placeholder="Max Height (In)" :required="false" :value="$product" />
                    </div>
                    <div class="col col-md-6">
                        <x-form.input name="max_width" placeholder="Max Width (In)" :required="false" :value="$product" />
                    </div>
                    <div class="col col-md-6">
                        <x-form.input name="weight" placeholder="Weight (lbs)" :required="false" :value="$product" />
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
