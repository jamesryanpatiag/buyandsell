<div class="container-fluid">
    <div class="row">
        <div class="col">
            <h4>Listing Summary</h4>
        </div>
    </div>
    <div class="container border border-dark text-xs">
        <div class="row">
            <div class="col text-end">PRICE</div>
        </div>
        </br>
        <div class="row">
            <div class="col">Year</div>
            <div class="col">Product Title</div>
        </div>
        <div class="row right-pane-container border border-dark">
            BORDER PLACEHOLDER
        </div>
        <div class="row">
            Short description
        </div>
        <div class="row">
            Location
        </div>
    </div>
    <div class="container text-xs fw-bold">
        <div class="row right-pane-divider ">
            CHOSEN SUBSCRIPTION PACKAGE
        </div>
        <div class="row right-pane-divider">
            {PACKAGE PLACEHOLDER}
        </div>
        <div class="row right-pane-divider">
            TOTAL:
            <div class="col text-end">000</div>
        </div>
    </div>
    <div class="row">
        <div class="col mt-3">
            <button type="button" class="btn btnSaveChanges btn-sm btn-outline-secondary">Save Changes</button>
        </div>
    </div>
</div>
