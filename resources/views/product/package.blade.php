@extends('product.__base')

@section('content_body')
    <div class="row">
        <div class="col-4">
            <div class="row">
                <div class="col-12">&nbsp;</div>
            </div>
        </div>
        <div class="col-8">
            <div class="row">
                @foreach($packages as $package)
                    <div class="col-md-4">
                        <a href="{{ route('product.create', ['package_id' => $package->id]) }}">SELECT {{ $package->name }}</a>
                        <div>{{ $package->duration }}</div>
                        <div>{{ $package->photo_limit }} PHOTOS</div>
                        <div>{{ $package->can_add_video }} </div>
                        <div>{{ $package->is_featured }}</div>
                        <div>{{ $package->is_premium }}</div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@push('css')
@endpush

@push('js')
@endpush

@push('modal')
@endpush
