@extends('product.__base')
@php
    /** @var \App\Models\Package $package */
    /** @var \App\Models\Member $profile */
@endphp
@section('content_body')
        <div class="row">
            <div class="main-panel col-md-8 col-xl-9">
                <form action="{{ route('product.store', ['package_id' => $package->id]) }}" class="row" id="productForm" method="post">
                    @csrf
                    <div class="accordion accordion-flush" id="accordionFlushExample">
                        <div id="part1" class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingOne">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="true" aria-controls="flush-collapseOne">
                                    1. Listing Information
                                </button>
                            </h2>
                            <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne">
                                <div class="accordion-body">
                                    @include("product.create.step-1")
                                </div>
                            </div>
                        </div>
                        <div id="part2" class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingTwo">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="true" aria-controls="flush-collapseTwo">
                                    2. Add Photos &amp; Videos&nbsp;<span class="dbz-error"></span>
                                </button>
                            </h2>
                            <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo">
                                <div class="accordion-body">
                                    @include("product.create.step-2")
                                </div>
                            </div>
                        </div>
                        <div id="part3" class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="true" aria-controls="flush-collapseThree">
                                    3. Contact Info
                                </button>
                            </h2>
                            <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree">
                                <div class="accordion-body">
                                    @include("product.create.step-3")
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="d-none">
                    <div id="dbz-template" class="dz-preview dz-file-preview col-12 col-md-6">
                        <div class="dz-details">
                            <div class="dz-filename"><span class="px-0" data-dz-name></span></div>
                            <div class="dz-size" data-dz-size></div>
                            <div class="position-relative dz-image-container">
                                <div class="product-cover" style="display:none;">
                                    <div class="form-check">
                                        <input class="form-check-input product_cover" type="radio" name="product_cover[]">
                                        <label class="form-check-label">
                                            Primary Photo
                                        </label>
                                    </div>
                                </div>
                                <img class="dz-image" data-dz-thumbnail />
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAW0AAAFtCAQAAABmCPOpAAAC9ElEQVR42u3SMQ0AAAzDsJU/6aHoU9kQouRgUiTA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZYG6yNtSXA2mBtsDZUPEkrAW5vc3dnAAAAAElFTkSuQmCC" class="img-fluid">
                                <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                            </div>
                            <div class="row pt-3">
                                <div class="dz-label col-12 pe-md-0 col-md-10">
                                    <div class="dz-error-message"><span data-dz-errormessage></span></div>
                                    <input type="text" class="form-control product-label" name="product_label[]" placeholder="Caption"/>
                                    <input type="hidden" class="product-photo" name="product_photo[]" value="0"/>
                                </div>
                                <div class="dz-buttons col-6 col-md-2 pt-3 pt-md-0 pt-md-0 d-flex justify-content-start justify-content-md-end align-items-center">
                                    <button type="button" style="display: none;" class="btn-dz-retry btn btn-primary me-3" data-dz-retry><i class="fas fa-redo"></i></button>
                                    <button type="button" class="btn-dz-delete btn btn-danger" data-dz-remove><i class="fas fa-times"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="dz-success-mark"><span>✔</span></div>
                        <div class="dz-error-mark"><span>✘</span></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xl-3">
                @include("product.create.right-pane")
            </div>
        </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ mix('css/create-product.css')}}">
@endpush

@push('js')
    <script>
        const product_photo_store_route = "{{ route('product-photo.store') }}";
        const product_photo_delete_route = "{{ route('product-photo.destroy',array("product_photo" => "replace")) }}";
        @php
            $old_photos = $profile->getProductPhotosProperties(old('product_photo', []));
            foreach($old_photos as $k => $p ){
                $old_photos[$k]['description'] = old('product_label',[])[$k];
            }
        @endphp
        const old_product_photos = {!! json_encode($old_photos) !!};
        const max_photos = {{ $package->photo_limit }};
    </script>
    <script src="{{ mix('js/create-product.js') }}" ></script>
@endpush

@push('modal')
    <div class="modal" id="createProductModal" tabindex="-1">
        <div class="modal-dialog modal-sm modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Confirmation</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-center">
                    <p class="text-center my-2">Are you sure you save?</p>
                    <p class="errorMessage" style="display:none"></p>
                </div>
                <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button class="btn text-white bg-brand-yellow-1 dbz-upload">Save Changes</button>
                </div>
            </div>
        </div>
    </div>
@endpush
