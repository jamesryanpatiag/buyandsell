@extends('profile.__base')
@section('content_body')
<div class="row">
    <h3 class="mb-5">Help &amp; Support</h3>
</div>
<div class="row">
    <h5 class="mb-3">Contact Us</h5>
    <p> If you have any concerns you can send us an email or give us a call.</p>
    <p>Email: {{ config('site.social.support_email') }}</p>
    <p>Mobile Number: {{ config('site.social.support_mobile') }}</p>
</div>
<br>
<div class="row">
    <h5 class="mb-3">FAQs</h5>
    <br><br>
</div>
<div class="row mb-4">
    <h6 class="mb-3">How to List Products</h6>
    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vulputate odio ut enim blandit volutpat. Dapibus ultrices in iaculis nunc sed augue lacus. Malesuada proin libero nunc consequat interdum varius sit amet mattis. Et egestas quis ipsum suspendisse ultrices gravida dictum. </p>
</div>
<div class="row mb-4">
    <h6 class="mb-3">Payment Method</h6>
    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vulputate odio ut enim blandit volutpat. Dapibus ultrices in iaculis nunc sed augue lacus. Malesuada proin libero nunc consequat interdum varius sit amet mattis. Et egestas quis ipsum suspendisse ultrices gravida dictum. </p>
</div>
<h1>&nbsp</h1>
@endsection
@push('css')
<link rel="stylesheet" href="{{ mix('css/profile.css')}}">
@endpush
