@extends('profile.__base')
@section('content_body')
<div class="row">
    <h3 class="mb-5">Contact Information</h3>
</div>
@if(session()->get('message'))
    <div class="row">
        <div class="col-12 mb-3">
            <div class="py-2 messageCont">
                {{ session()->get('message') }}
            </div>
        </div>
    </div>
@endif
<form class="settings-form" action="{{ route('profile.contact-information.update') }}" method="post">
    @method('PATCH')
    @csrf
    <div class="row">
        <div class="row">
            <div class="col col-md-6">
                <x-form.input name="mobile_number" label="Mobile Number" placeholder="Mobile Number" :value="$profile" required/>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col col-md-6">
                <livewire:member-email :new="false" placeholder="Email" :email="$profile->email"/>
            </div>
        </div>
        <div class="row setting-actions justify-content-md-center mb-5">
            <div class="col-md-auto">
                <button type="submit" class="btn btn-sm btn-outline-secondary">Save Changes</button>
            </div>
            <div class="col-md-auto">
                <a href="{{ route('profile.index') }}" class="btn btn-sm btn-outline-secondary">Cancel</a>
            </div>
        </div>
    </div>
</form>
@endsection
@push('css')
    <link rel="stylesheet" href="{{ mix('css/profile.css')}}">
@endpush
