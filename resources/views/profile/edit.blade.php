@extends('profile.__base')
@section('content_body')
<div class="row">
    <h3 class="mb-5"> Edit Profile </h3>
</div>
<x-form-validation-error-messages />
<form class="settings-form" action="{{ route('profile.update') }}" method="post">
    @method('PATCH')
    @csrf
    <div class="row mb-5">
        <div class="col-md-3">
            <h5 class="mb-2"> Profile Photo </h5>
            <div class="edit-picture">
                <img src="{{ $profile->profile_photo }}" alt="profile picture" class="img-fluid profile_picture_img" />
            </div>
        </div>
        <div class="col-md-9 align-self-center">
            <p>Clear photos are important way for buyers and sellers to learn about each other.</p>
            <input type="hidden" name="profile_photo" />
            <button type="button" class="btn btn-sm btn-outline-secondary" data-bs-toggle="modal" data-bs-target="#croppieModal">Upload a photo</button>
        </div>
    </div>
    <div class="row">
        <h5 class="mb-2"> Public Profile </h5>
        <div class="row">
            <div class="col">
                <x-form.input name="first_name" :value="$profile" placeholder="First Name" />
            </div>
            <div class="col">
                <x-form.input name="last_name" :value="$profile" placeholder="Last Name" />
            </div>
        </div>
        <div class="row">
            <div class="col">
                <x-form.input name="company_name" :value="$profile" placeholder="Company Name" />
            </div>
        </div>
        <div class="row">
            <div class="col">
                <x-form.textarea name="bio" label="Bio ( maximum of 255 characters)" :value="$profile" placeholder="Bio" />
            </div>
        </div>
        <livewire:member-location :value="$profile->location_province_id ?? null" />
        <div class="row setting-actions justify-content-md-center mb-5">
            <div class="col-md-auto">
                <button type="submit" class="btn btn-sm btn-outline-secondary">Save Changes</button>
            </div>
            <div class="col-md-auto">
                <a href="{{ route('profile.index') }}" class="btn btn-sm btn-outline-secondary">Cancel</a>
            </div>
        </div>
    </div>
</form>
@endsection
@push('modal')
    <div class="modal" tabindex="-1" id="croppieModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Crop Photo</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="fileupload mb-5">
                        <label class="form-label my-0" for="profile_photo_upload">Select Picture</label>
                        <input class="form-control form-control-sm" id="profile_photo_upload" type="file" accept="image/png, image/jpeg">
                    </div>
                    <div class="upload-croppie-container" style="display:none;">
                        <div id="upload-croppie"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn text-white bg-brand-yellow-1" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary upload-result">Crop</button>
                </div>
            </div>
        </div>
    </div>
@endpush
@push('js')
    <script src="{{ asset('js/croppie.min.js') }}" ></script>
    <script src="{{ mix('js/edit-profile.js') }}" ></script>
@endpush
@push('css')
    <link rel="stylesheet" href="{{ mix('css/profile.css')}}">
    <link rel="stylesheet" href="{{ mix('css/edit-profile.css')}}">
@endpush
