<x-app-layout>
    <div class="container">
        <div class="row main-pane">
            <div class="profile col-xl-3 left-pane-bg mb-3">
                <h3 class="d-none d-xl-block mb-5">&nbsp</h3>
                <ul class="settings-list-group list-group pt-4 " >
                    <li class="list-group-item"><a href="{{ route('profile.index') }}" class="text-decoration-none">Profile Information</a></li>
                    <li class="list-group-item @if( in_array(Route::currentRouteName(),array("profile.edit") )) active @endif"><a href="{{ route('profile.edit') }}" class="text-decoration-none">Edit Profile</a></li>
                    <li class="list-group-item {{ Request::path() ==  'profile/help' ? 'active' : ''  }}"><a href="{{ url('profile/help') }}" class="text-decoration-none">Help &amp; Support</a></li>
                    <li class="list-group-item @if( in_array(Route::currentRouteName(),array("profile.password.edit") )) active @endif"><a href="{{ route('profile.password.edit') }}" class="text-decoration-none">Password &amp; Security</a></li>
                    <li class="list-group-item @if( in_array(Route::currentRouteName(),array("profile.contact-information.edit") )) active @endif"><a href="{{ route('profile.contact-information.edit') }}" class="text-decoration-none">Contact Information</a></li>

                </ul>

            </div>
            <div class="profile col-xl-9 right-pane-bg ">
                <div class="container pt-4 right-container">
                    @yield('content_body')
                </div>
            </div>
        </div>
        @push('css')
        <link rel="stylesheet" href="{{ mix('css/dashboard.css')}}">
        @endpush
</x-app-layout>
