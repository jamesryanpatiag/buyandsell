<x-app-layout>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="profile-banner">

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-3">

            </div>
            <div class="col-xl-9">
                <nav class="navbar navbar-expand-xl profile-topnavbar">
                    <div class="container-fluid">
                        <div class="collapse navbar-collapse justify-content-between" id="navbarProfile">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#">Listings</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Purchased</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Sold</a>
                                </li>

                            </ul>
                            <div class="d-flex">
                                <a href="{{ route('profile.edit') }}" class="btn-profile btn btn-sm btn-outline-secondary">Edit Profile</a>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-2">

            </div>
            <div class="col-xl-10">
                <hr class="profile-hr">
            </div>
        </div>
        <div class="row">
            <div class="profile col-xl-3">
                <div class="profile-picture">
                    <img src="{{ $profile->profile_photo }}" alt="profile picture" class="img-fluid"/>
                </div>
                <div class="profile-information d-flex flex-column">
                    <span class="profile-name lh-1 mb-2">{{ $profile->full_name }}</span>
                    <span class="profile-handler lh-1">{{ "@".$profile->username }}</span>
                    <span class="profile-rate mt-2 mb-1">
                        <div class="rate d-flex align-items-center">
                            <i class="me-1 fas fa-star active"></i>
                            <i class="me-1 fas fa-star active"></i>
                            <i class="me-1 fas fa-star active"></i>
                            <i class="me-1 fas fa-star active"></i>
                            <i class="me-2 fas fa-star"></i>
                            <span class="rate-text">4.4 ratings</span>
                        </div>
                    </span>
                    <span class="profile-location mt-2">
                        {{ $profile->province }}
                    </span>
                    <span class="profile-verification mt-2 d-flex align-items-center">
                        <span class="verification-status me-2">{{ $profile->email_verified_status }}</span>
                        <span class="verification-email d-flex align-items-center justify-content-center"><i class="far fa-envelope"></i></span>
                    </span>
                    <p class="profile-bio mt-3">
                        {{ $profile->bio }}
                    </p>
                    <div class="profile-follow-cont d-flex justify-content-between">
                        <span class="profile-followers">250 Followers</span>
                        <span class="profile-following">1235 Following</span>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 pt-3">
{{--                CONTAINER--}}
                    <div class="profile-container">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col">
                                    <h3>Listings</h3>
                                </div>
                                <div class="col">
                                    <form class="search-listing-form">
                                    @csrf
                                    <div class="row d-flex justify-content-end">
                                        <div class="col-auto">
                                            <div class=" input-group input-group-listing-search">
                                                <input type="text" class="form-control form-control-sm" name="search-list" placeholder="Search Listings" aria-label="" aria-describedby="button-addon2">
                                                <button class="btn btn-outline-secondary btn-sm btn-listing-search" type="button" id="button-addon2"><i class="fas fa-search"></i></button>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <button type="submit" class="btn btn-sm btn-outline-secondary mb-3">Filters <i class="ms-2 fas fa-sliders-h"></i></button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="row list">
                                        @foreach($profile->products as $product)
                                            <x-product-item :product="$product" />
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
{{--                END CONTAINER--}}
            </div>
        </div>
    </div>

    @push('css')
        <link rel="stylesheet" href="{{ mix('css/dashboard.css')}}">
    @endpush
</x-app-layout>
