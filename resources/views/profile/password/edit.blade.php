@extends('profile.__base')
@section('content_body')
<div class="row">
    <h3 class="mb-5">Password &amp; Security</h3>
</div>
@if(session()->get('message'))
    <div class="row">
        <div class="col-12 mb-3">
            <div class="py-2 messageCont">
                {{ session()->get('message') }}
            </div>
        </div>
    </div>
@endif
<form class="settings-form" action="{{ route('profile.password.update') }}" method="post">
    @method('PATCH')
    @csrf
    <div class="row">
        <div class="row">
            <div class="col col-md-6">
                <x-form.input type="password" name="old_password" label="Old Password" placeholder="Old Password" required/>
            </div>
        </div>
        <div class="row">
            <div class="col col-md-6">
                <x-form.input type="password" name="new_password" label="New Password" placeholder="New Password" required/>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col col-md-6">
                <x-form.input type="password" name="new_password_confirmation" label="Confirm Password" placeholder="Confirm Password" required/>
            </div>
        </div>
        <div class="row setting-actions justify-content-md-center mb-5">
            <div class="col-md-auto">
                <button type="submit" class="btn btn-sm btn-outline-secondary">Save Changes</button>
            </div>
            <div class="col-md-auto">
                <a href="{{ route('profile.index') }}" class="btn btn-sm btn-outline-secondary">Cancel</a>
            </div>
        </div>
    </div>
</form>
@endsection
@push('css')
    <link rel="stylesheet" href="{{ mix('css/profile.css')}}">
@endpush
