@extends('components.form.__base')
@section('content')
    <textarea name="{{$name}}" id="{{$id}}" {{ $attributes->merge(['class' => 'form-control']) }} type="{{$type}}" {{$required ? 'required' : ''}} >{{ $value }}</textarea>
@overwrite
