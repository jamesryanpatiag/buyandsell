@extends('components.form.__base')
@section('content')
    <div class="form-check">
        &nbsp;<br>
        <input class="form-check-input" name="{{$name}}" id="{{$id}}" {{ $attributes->merge() }} type="checkbox" {{$required ? 'required' : ''}} value="1" @if($value == '1') checked @endif/>
        <label class="form-check-label" for="{{$id}}">
            {{$label}} <span class='text-danger'>{{$required ? "*" : ''}}</span>
        </label> <br />
    </div>
@overwrite
