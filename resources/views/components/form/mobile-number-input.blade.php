@extends('components.form.__base')
@section('content')
    <div class="fb-input-group d-flex">
        <div class="fbig1 d-inline-block float-start">
            <input type="text" name="{{ $prefix_name }}" class="form-control" id="{{ $prefix_id }}" placeholder="+65" maxlength="10" required value="{{ $prefix_value }}"/>
        </div>
        <div style="" class="fbig2 d-inline-block float-end">
            <input type="text" name="{{$name}}" id="{{$id}}" {{ $attributes->merge(['class' => 'form-control']) }} aria-label="phone" aria-describedby="{{ $prefix_id}}" {{$required ? 'required' : ''}} value="{{ $value }}">
        </div>
    </div>
@overwrite
