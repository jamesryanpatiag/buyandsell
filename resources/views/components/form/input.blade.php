@extends('components.form.__base')
@section('content')
    <input name="{{$name}}" id="{{$id}}" {{ $attributes->merge(['class' => 'form-control']) }} type="{{$type}}" {{$required ? 'required' : ''}} value="{{ $value }}" />
@overwrite
