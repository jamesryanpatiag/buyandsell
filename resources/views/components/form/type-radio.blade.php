@extends('components.form.__base')

@section('content')
    @foreach($options as $key => $l)
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="type" id="type{{$key}}" value="{{ $l }}"
            {!! $selected($key, $l) ? 'checked="checked"' : '' !!}
            >
            <label class="form-check-label" for="type{{ $key }}">{{ $l }}</label>
        </div>
    @endforeach
@overwrite
