<div class="col-12 @stack('base_class') mb-3 component-form-container @if(!in_array($type,array('checkbox','radio'))) component-form-container-2 @endif" id="{{ $id }}_div_container">
    @if(in_array($type,array('checkbox','radio')))
        @yield('content')
        @error($name)
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    @else
{{--        <label for="{{$id}}" class="form-label my-0">{{$label}} {{$required ? "*" : ''}}</label><br />--}}
        @if($notes ?? false)
        <span class="text-gold form-label-sub mb-3">{{ $notes }}</span>
        @endif
        @if($prefix || $suffix)
        <div class="input-group">
        @endif
                @if($prefix)
                    <span class="input-group-text" id="basic-addon2">{{$prefix}}</span>
                @endif
                <div class="position-relative @if($prefix) prefix @endif @if($suffix) suffix @endif">
                @yield('content')
                @if($errorMessage ?? false)
                        <div class="alert alert-danger">{{ $errorMessage }}</div>
                @else
                        @error($name)
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                @endif
                @if($successMessage ?? false)
                        <div class="alert alert-success">{{ $successMessage }}</div>
                @endif
                </div>
            @if($suffix)
                <span class="input-group-text" id="basic-addon2">{{$suffix}}</span>
            @endif
        @if($prefix || $suffix)
        </div>
        @endif
    @endif
</div>
