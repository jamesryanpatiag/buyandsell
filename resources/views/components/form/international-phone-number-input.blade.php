@extends('components.form.input')

@push('css')
    <link rel="stylesheet" href="{{asset('libs/intTelInput/intlTelInput.min.css')}}">
@endpush

@push('js')
    <script src="{{ asset("libs/intTelInput/intlTelInput.js") }}" ></script>
    <script>
        function restrictInputForMobileNumber(inputElement) {
            inputElement.on('keypress', function(event){
                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode !== 45 && event.keyCode !== 43))
                    return false;
            });
        }
        $(document).ready(function () {
            let input = document.querySelector("#{{ $id }}");
            window.intlTelInput(input, {
                nationalMode: false, preferredCountries: ["hk", "id", "kr", "my", "ph", "sg", "tw", "th", "vn"], initialCountry: "sg",
            });
            restrictInputForMobileNumber($('#{{ $id }}'));
        });
    </script>
@endpush
