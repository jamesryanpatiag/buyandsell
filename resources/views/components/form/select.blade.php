@extends('components.form.__base')

@section('content')
    <select name="{{$name}}" id="{{$id}}" {{$required ? 'required' : ''}} {{ $attributes->merge(['class' => 'form-select']) }} >
        <option value="">{{ $attributes['placeholder'] ?? '' }}</option>
        @foreach($options as $key => $l)
            <option
                @if(!empty($filter))data-filter="{{ $optionsAreModels ? $l->{$filter} : '' }}"@endif
                {{ $selected($key, $l) ? 'selected' : ''}}
                value="{{ $optionsAreModels ? $l->id : ($optionsAreAssociative ? $key : $l) }}"
            >{{ $optionsAreModels ? $l->name : $l}}</option>
        @endforeach
    </select>
@overwrite
