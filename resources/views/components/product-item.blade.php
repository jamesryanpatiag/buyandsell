<div {{ $attributes->merge(['class' => 'list-item mb-3 col-6 col-md-4 col-xl-3 d-flex flex-column']) }}>
    <a href="{{ route('product.edit',['product'=>$product->id]) }}" class="list-image-container" >{{$product->main_image()}}</a>
    <a href="{{ route('product.edit',['product'=>$product->id]) }}" class="text-decoration-none"><span class="list-name">{{ $product->title }}</span></a>
    <span class="list-price">PHP {{ $product->price }}</span>
    <span class="list-heart"><a class="me-1" href="#"><i class="far fa-heart"></i></a> 7</span>
</div>
