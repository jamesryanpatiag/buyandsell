<div class="card">
    <div class="card-header font-weight-bold font-xl px-3 border-xs">
        {{ $summaryReportData->getTitle() }}
        {{--                    <a href="#" class="btn btn-default">Download</a>--}}
    </div>
    <div class="card-body p-0">
        <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover table-md mb-0">
            <thead>
            <tr>
                @foreach($summaryReportData->getColumns() as $column)
                    <th>{{ ucwords(str_replace(['_'], ' ', $column)) }}</th>
                @endforeach
                    @foreach($summaryReportData->getAdditionalColumns() as $column)
                        <th>{{ ucwords(str_replace(['_'], ' ', $column)) }}</th>
                    @endforeach
            </tr>
            </thead>
            <tbody>
            @foreach($summaryReportData->getData() as $row)
                <tr>
                    @foreach($summaryReportData->getColumns() as $column)
                        <td>{{ $row->{$column} }}</td>
                    @endforeach
                    @php
                        $additionaldata = $summaryReportData->getAdditionalData($row);
                    @endphp
                        @foreach($summaryReportData->getAdditionalColumns() as $column)
                            <td>{{ $additionaldata[$column] }}</td>
                        @endforeach
                </tr>
            @endforeach
            @if($summaryReportData->hasTotals())
                <tr>
                    @foreach($summaryReportData->getColumns() as $column)
                        @if($loop->first)
                            <td>
                                Total
                            </td>
                            @continue
                        @endif
                        @if($summaryReportData->hasTotal($column))
                            <td>{{ $summaryReportData->getTotals()[$column] }}</td>
                        @else
                            <td>&nbsp;</td>
                        @endif
                    @endforeach
                </tr>
            @endif

            </tbody>
        </table>
        </div>
    </div>
</div>
