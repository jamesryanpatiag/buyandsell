@if ($errors->any())
    <div class="alert alert-danger position-relative">
        <div class="d-flex justify-content-between align-items-center">
            <div>
                {{ session('message', 'Form has errors.') }}
                <a href="javascript:;" class="btn btn-danger btn-sm" onClick="(function(event){
                    if(event.target.parentNode.parentNode.nextElementSibling.style.display == 'block'){
                        event.target.innerHTML = 'Open';
                        event.target.parentNode.parentNode.nextElementSibling.style.display = 'none';
                    } else {
                        event.target.innerHTML = 'Close';
                        event.target.parentNode.parentNode.nextElementSibling.style.display = 'block';
                    }
                    return false;
                })(event);return false;">Open</a>
            </div>
            <div class=""><a href="javascript:;" class="text-decoration-none fw-bold btn-danger btn-sm" onClick="(function(event){
                event.target.parentNode.parentNode.parentNode.remove();
            })(event);return false;">&times;</a></div>
        </div>
        <div style="display:none;" class="form-validation-error-list-container">
            <ul class="form-validation-error-list">
                @foreach ($errors->toArray() as $name => $error)
                    <li><a href="#" onclick="(function(event){
                                const rect = document.getElementsByName('{{ $name }}')[0].getBoundingClientRect();
                                window.scrollTo(rect.x, rect.y)
    })(event);return false;">{{ $error[0] }}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
@elseif(session('message', false))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif
