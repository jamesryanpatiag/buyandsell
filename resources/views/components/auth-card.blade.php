<div class="container">
    <div class="form-auth">
        <div class="row flex flex-column justify-content-center mb-2">
            <div class="col text-center">
                {{ $logo }}
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="mt-6 px-6 py-4 bg-white">
                    {{ $slot }}
                </div>
            </div>
        </div>
    </div>
</div>
@push('css')
    <link rel="stylesheet" href="{{ mix('css/auth.css')}}">
@endpush
@push('js')
    <script src="{{ mix('js/register.js') }}" ></script>
@endpush
