@props(['errors'])

@if ($errors->any())

        <ul class="-ml-6 -mt-2 mb-1 text-danger list-none text-xs error-inline">
            @foreach ($errors->all() as $error)
                <li><i class="fas fa-exclamation-circle"></i> {{ $error }}</li>
            @endforeach
        </ul>
@endif
