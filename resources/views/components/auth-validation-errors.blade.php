@props(['errors'])

@if ($errors->any())
    <div {!! $attributes->merge(['class' => 'alert alert-danger']) !!} role="alert">
        <h4 class="alert-heading">
            {{ __('Whoops! Something went wrong.') }}
        </h4>
        <hr>
        <ul class="mt-3 fs-6">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
