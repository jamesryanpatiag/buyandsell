<div class="form-auth container main-pane montserrat">
    <div class="row flex flex-column justify-content-center my-2">
        <div class="col text-center">
            {{ $logo }}
        </div>
    </div>

    <div class="mt-6 px-6 row text-center">
        {{ $slot }}
    </div>
</div>
@push('css')
    <link rel="stylesheet" href="{{ mix('css/auth.css')}}">
@endpush
@push('js')
    <script src="{{ mix('js/register.js') }}" ></script>
@endpush
