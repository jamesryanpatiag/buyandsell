@component('mail::message')
    Your registration was successful
    <br>
    <br>
    Thanks,
    <br>
    {{ config('app.name') }}
@endcomponent
