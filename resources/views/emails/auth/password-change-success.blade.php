@component('mail::message')
    your password was changed

    Thanks,
    {{ config('app.name') }}
@endcomponent
