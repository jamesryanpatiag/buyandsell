@component('mail::message')
    <center>
    <h1 style="text-align:center;font-size:24px;">Hello {{ $user->username ?? 'User' }},</h1>
    <br>
    A request has been received to change
    <br>
    the password for your account.
    @component('mail::button', ['color'=>'primary', 'style'=>'font-size:20px;', 'url'=> $resetUrl])
        Reset Password
    @endcomponent
    If you did not initiate this request,
    <br>
    please contact us immediately at
    <br>
    <a href="mailto:{{ config('site.support.email')  }}">{{ config('site.support.email')  }}</a>
    </center>
@endcomponent
