@component('mail::message')
    <center>
    <h1 style="text-align:center;font-size:24px;">Email Confirmation</h1>
    <br>
    Hey {{ $user->username }}, you're almost ready to start
    <br>
    enjoying Equipment Exchange.
    <br>
    <br>
    Simply click the big button below to verify your email address.
    @component('mail::button',['color'=>'primary','style'=>'font-size:20px;','url'=>$verificationUrl])
        Verify email address
    @endcomponent
    </center>
@endcomponent
