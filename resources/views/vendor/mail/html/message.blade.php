@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
{{ config('app.name') }}
@endcomponent
@endslot

{{-- Body --}}
{{ $slot }}

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
<a href="{{ config('site.social.facebook') }}"><img src="{{ asset("img/email/facebook.png") }}" class="soc-icon" alt=""></a>&nbsp;&nbsp;<a href="{{ config('site.social.instagram') }}"><img src="{{ asset("img/email/instagram.png") }}" class="soc-icon" alt=""></a>&nbsp;&nbsp;<a href="{{ config('site.social.linkedin') }}"><img src="{{ asset("img/email/linkedin.png") }}" class="soc-icon" alt=""></a>
<br>
<br>
marketing@iconequipments.com.ph
<br>
0917-638-2039
<br>
KM 12 Merville Access Road, Merville
<br>
Parañaque City
@endcomponent
@endslot
@endcomponent
