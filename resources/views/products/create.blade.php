@extends('products.__base')

@section('content_body')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-xl-9">
            <form action="/create-product" class="row" id="product-dropzone">
                @csrf
                <div class="accordion accordion-flush" id="accordionFlushExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="true" aria-controls="flush-collapseOne">
                                1. Listing Information
                            </button>
                        </h2>
                        <div id="flush-collapseOne" class="accordion-collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">
                                @include("products.create.step-1")
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingTwo">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="true" aria-controls="flush-collapseTwo">
                                2. Add Photos &amp; Videos
                            </button>
                        </h2>
                        <div id="flush-collapseTwo" class="accordion-collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">
                                @include("products.create.step-2")
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="true" aria-controls="flush-collapseThree">
                                3. Contact Info
                            </button>
                        </h2>
                        <div id="flush-collapseThree" class="accordion-collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">
                                @include("products.create.step-3")
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <button type="button" class="btn btn-primary dbz-upload">Submit(TEST)</button>
                </div>
            </form>
            <div class="d-none">
                <div id="dbz-template" class="dz-preview dz-file-preview col-12 col-md-6">
                    <div class="dz-details">
                        <div class="row">
                            <div class="dz-label col-10 pe-0">
                                <div class="dz-error-message"><span data-dz-errormessage></span></div>
                                <input type="text" class="form-control" name="product_label[]" placeholder="Label"/>
                            </div>
                            <div class="col-2 d-flex justify-content-center align-items-center">
                                <button type="button" class="btn-dz-delete btn btn-danger" data-dz-remove>&times;</button>
                            </div>
                        </div>
                        <div class="dz-filename"><span class="px-0" data-dz-name></span></div>
                        <div class="dz-size" data-dz-size></div>
                        <div class="position-relative">
                            <img class="dz-image" data-dz-thumbnail />
                            <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                        </div>
                    </div>
                    <div class="dz-success-mark"><span>✔</span></div>
                    <div class="dz-error-mark"><span>✘</span></div>

                </div>
            </div>
            </div>
            <div class="col-md-4 col-xl-3">
                @include("products.create.right-pane")
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ mix('css/create-product.css')}}">
@endpush

@push('js')
    <script src="{{ mix('js/create-product.js') }}" ></script>
@endpush
