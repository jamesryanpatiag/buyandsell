<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-md-6">
            <x-form.input placeholder="First Name" name="first_name" />
        </div>
        <div class="col-12 col-md-6">
            <x-form.input placeholder="Last Name" name="last_name" />
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-8">
            <x-form.input placeholder="Address" name="address" />
        </div>
        <div class="col-12 col-md-4">
            <x-form.input placeholder="City" name="city" />
        </div>
    </div>
    <div class="row">
        <div class="col">
            <x-form.input placeholder="Contact Number" name="contact_number" />
        </div>
    </div>
</div>
