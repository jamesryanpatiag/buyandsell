<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="dropzone">
                <div id="dropzone-previews" class="row">
                    <div class="dz-default dz-message col-12"><button class="dz-button" type="button">Drop files here to upload</button></div>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-12">
                    <x-form.input name="youtube_url" placeholder="Youtube Link" />
                </div>
            </div>
        </div>
    </div>
</div>
