<div class="container-fluid">
    <div class="row">
        <form class="settings-form" action="#" method="post">
            @csrf
            <div class="row">
                <div class="row">
                    <div class="col">
                        <x-form.input name="product_title" placeholder="Product Title" required />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.input name="description" placeholder="Description" required />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.textarea name="price" placeholder="Price (PHP)" required />
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox" id="rental" checked>
                            <label class="form-check-label" for="rental">Rental</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.input name="model" placeholder="Model" required />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.select name="brand" placeholder="Brand" :options="[]" />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.select name="industries" placeholder="Industries" :options="[]" />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.select name="class" placeholder="Class" :options="[]" />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.select name="condition" placeholder="Condition" :options="[]" />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.select name="status" placeholder="Status of the Product" :options="[]" />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.select name="year" placeholder="Year" :options="[]" />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.select name="mileage" placeholder="Equipment Mileage" :options="[]" />
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-6">
                        <x-form.select name="additional" placeholder="Additional Information" :options="[]" />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.input name="serial" placeholder="Serial Number" required />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.input name="plateNo" placeholder="Plate Number" required />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.input name="engine_make" placeholder="Engine Make" required />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.input name="hrs_of_use" placeholder="Hours of Use (hr)" required />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.select name="fuel_type" placeholder="Fuel Type" :options="[]" />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.input name="front_tire_size" placeholder="Front Tire Size" required />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.input name="rear_tire_size" placeholder="Rear Tire Size" required />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.input name="max_height" placeholder="Max Height (In)" required />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.input name="max_width" placeholder="Max Width (In)" required />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <x-form.input name="weight" placeholder="Weight (lbs)" required />
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
