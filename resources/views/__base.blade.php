<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags -->
    @stack('head')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">

{{--    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('apple-touch-icon.png') }}">--}}
{{--    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon-32x32.png') }}">--}}
{{--    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon-16x16.png') }}">--}}
{{--    <link rel="manifest" href="{{ asset('site.webmanifest') }}">--}}
{{--    <link rel="mask-icon" href="{{ asset('safari-pinned-tab.svg') }}" color="#5bbad5">--}}
{{--    <meta name="msapplication-TileColor" content="#da532c">--}}
{{--    <meta name="theme-color" content="#ffffff">--}}

{{--    <!-- Primary Meta Tags -->--}}
{{--    <?php--}}
{{--    $title = "Icon Buy & Sell";--}}
{{--    $description = "";--}}
{{--    ?>--}}
{{--    <title>@stack('title',$title)</title>--}}
{{--    <meta name="title" content="@stack('title',$title)">--}}
{{--    <meta name="description" content="@stack('description',$description)">--}}

{{--    <!-- Open Graph / Facebook -->--}}
{{--    <meta property="og:type" content="website">--}}
{{--    <meta property="og:url" content="{{ url()->full() }}">--}}
{{--    <meta property="og:title" content="@stack('title',$title)">--}}
{{--    <meta property="og:description" content="@stack('description',$description)">--}}
{{--    <meta property="og:image" content="{{ asset("images/web_preview.png") }}">--}}

{{--    <!-- Twitter -->--}}
{{--    <meta property="twitter:card" content="summary_large_image">--}}
{{--    <meta property="twitter:url" content="{{ url()->full() }}">--}}
{{--    <meta property="twitter:title" content="@stack('title',$title)">--}}
{{--    <meta property="twitter:description" content="@stack('description',$description)">--}}
{{--    <meta property="twitter:image" content="{{ asset("images/web_preview.png") }}">--}}
    <link rel="stylesheet" href="{{ mix('css/base.css')}}">
    @stack('css')
    @livewireStyles
</head>
<body class="min-vh-100">
    <div class="d-flex w-100 flex-column min-vh-100">
        @include('header')
        @yield('body')
        @include('footer')
    </div>
    @if(\Auth::guard('member')->check())
    <div class="modal" id="logoutModal" tabindex="-1">
        <div class="modal-dialog modal-sm modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Confirmation</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p class="text-center my-2">Are you sure you want to logout?</p>
                </div>
                <div class="modal-footer">
                    <form name="formLogout" action="{{ route('logout') }}" method="post">
                        @csrf
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button class="btn text-white bg-brand-yellow-1">Logout</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endif
    @stack('modal')

    <script src="{{ mix('js/app.js') }}" ></script>
    @stack('js')
    @livewireScripts
</body>
</html>
