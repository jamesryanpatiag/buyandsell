<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="" />
            </a>
        </x-slot>

        <!-- Validation Errors -->

        <form id="frmRegister" method="POST" action="{{ route('register') }}">
            @csrf
            <div class="row">
                <div class="col">
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                </div>
            </div>
            <!-- Name -->

            <livewire:member-username />
            <livewire:member-email />


            <!-- Password -->
            <div class="row">
                <div class="col mb-3">
                    <x-label for="password" class="form-label" :value="__('Password')" />
                    <x-input id="password" class=""
                                    type="password"
                                    name="password"
                                    required autocomplete="new-password" />
                </div>
            </div>

            <!-- Confirm Password -->
            <div class="row">
                <div class="col mb-3">
                    <x-label for="password_confirmation" class="form-label" :value="__('Confirm Password')" />
                    <x-input id="password_confirmation" class=""
                                    type="password"
                                    name="password_confirmation" required />
                </div>
            </div>


            <livewire:member-location />
            <div class="row">
                <div class="col">
                    <div class="d-flex justify-content-end">
                        <a class="btn btn-link fs-6 text-gray-600" href="{{ route('login') }}">
                            {{ __('Already registered?') }}
                        </a>

                        <x-button class="btn btn-primary ms-3">
                            {{ __('Register') }}
                        </x-button>
                    </div>
                </div>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
