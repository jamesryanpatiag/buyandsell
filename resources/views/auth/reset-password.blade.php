<x-guest-layout>
    <x-password-card>
        <x-slot name="logo">
            <a href="/">
                <i class="fas fa-lock-open fa-4x text-brand-yellow-1"></i>
            </a>
        </x-slot>
        <h3> Reset Password </h3>
        <p> You've successfully verified your account. Enter new password below. </p>
        <form method="POST" action="{{ route('password.update') }}">
            @csrf

            <!-- Password Reset Token -->
            <input type="hidden" name="token" value="{{ $request->route('token') }}">

            <!-- Email Address -->
            <x-input id="email" type="hidden" name="email" value="{{ $request->email }}" />
            <!-- Password -->
            <div class="row d-flex justify-content-center">
                <div class="col col-sm-9 mb-1">
                    <x-form.input id="password" class="py-2" placeholder="Password" type="password" name="password" required />
                </div>
            </div>

            <!-- Confirm Password -->
            <div class="row d-flex justify-content-center">
                <div class="col col-sm-9 mb-1">
                    <x-form.input id="password_confirmation" class="py-2" placeholder="Confirm Password" type="password" name="password_confirmation" required />
                </div>
            </div>
{{--            <!-- Validation Errors -->--}}
{{--            <div class="row">--}}
{{--                <div class="col">--}}
{{--                    <x-forgot-password-errors class="mb-4" :errors="$errors" />--}}
{{--                </div>--}}
{{--            </div>--}}
            <div class="row">
                <div class="col">
                    <div class="d-flex justify-content-center">
                        <x-button class="btn btn-primary w-70 bg-brand-yellow-1 border-0 py-3 btn-rounded shadow-sm">
                            {{ __('Reset Password') }}
                        </x-button>
                    </div>
                </div>
            </div>
        </form>
    </x-password-card>
</x-guest-layout>
