<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <div class="mb-4 fs-6 text-gray-600">
            {{ __('This is a secure area of the application. Please confirm your password before continuing.') }}
        </div>

        <form method="POST" action="{{ route('password.confirm') }}">
            @csrf

            <!-- Validation Errors -->
            <div class="row">
                <div class="col">
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                </div>
            </div>

            <!-- Password -->
            <div class="row">
                <div class="col mb-3">
                    <x-label for="password" :value="__('Password')" />

                    <x-input id="password" class="block mt-1 w-full"
                                    type="password"
                                    name="password"
                                    required autocomplete="current-password" />
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="d-flex justify-content-end">
                        <x-button class="btn btn-primary">
                            {{ __('Confirm') }}
                        </x-button>
                    </div>
                </div>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
