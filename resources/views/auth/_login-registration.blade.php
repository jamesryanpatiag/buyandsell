@php
    $is_registration = $is_registration ?? false;
@endphp

<x-guest-layout>
    <div class="custom-container container @if($is_registration) right-panel-active @endif" id="container">
        <div class="form-container sign-up-container">
            <form class="login-registration-form" method="POST" action="{{ route('register') }}">
                @csrf
                <h1 class="mb-3">Create Account</h1>
{{--                @if($is_registration)--}}
{{--                    <x-form-validation-error-messages />--}}
{{--                @endif--}}
                <x-form.input name="username" :value="old('username')" autofocus placeholder="Username" />
                <x-form.input type="email" name="email" :value="old('email')" autofocus placeholder="Email" />
                <x-form.input
                        type="password"
                         name="password"
                         placeholder="Password"
                         autocomplete="new-password" />
                <x-form.input
                         type="password"
                         placeholder="Confirm Password"
                         name="password_confirmation" />
                <livewire:member-location />
                <input type="hidden" name="form_type" value="registration" />
                <x-button class="btn btn-yellow mt-3">
                    {{ __('Sign Up') }}
                </x-button>
            </form>
        </div>
        @php
        \App\View\Components\Form\BasicFormComponent::resetFormElementsList();
        @endphp
        <div class="form-container sign-in-container">
            <form class="login-registration-form" method="POST" action="{{ route('login') }}">
                @csrf
                <h2 class="fw-bolder mb-5 text-brand-yellow-1">Sign in to continue</h2>
{{--                @if(!$is_registration)--}}
{{--                    <x-form-validation-error-messages />--}}
{{--                @endif--}}
                <x-form.input name="username" autofocus placeholder="Username"/>
                <x-form.input name="password" placeholder="Password"
                         type="password"
                         autocomplete="current-password" />
                <input type="hidden" name="form_type" value="login" />
                <x-button class="btn btn-yellow">
                    {{ __('Sign In') }}
                </x-button>
                <a class="sub-a mt-3 text-decoration-none" href="{{ route('password.request') }}">Forgot your password?</a>
            </form>
        </div>
        <div class="overlay-container">
            <div class="overlay">
                <div class="overlay-panel overlay-left">
                    <h1>Welcome Back!</h1>
                    <p>To keep connected with us please login with your personal info</p>
                    <button class="btn btn-outline-light ghost" id="signIn">Sign In</button>
                </div>
                <div class="overlay-panel overlay-right">
                    <h1>Hello, Friend!</h1>
                    <p>Enter your personal details and start journey with us</p>
                    <button class="btn btn-outline-light ghost" id="signUp">Sign Up</button>
                </div>
            </div>
        </div>
    </div>
    @push('css')
        <link rel="stylesheet" href="{{ mix('css/login-registration.css')}}">
    @endpush

    @push('js')
        <script src="{{ mix('js/login-registration.js') }}" ></script>
    @endpush

</x-guest-layout>
