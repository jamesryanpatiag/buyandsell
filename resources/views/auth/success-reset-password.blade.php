<x-guest-layout>
    <x-password-card>
        <x-slot name="logo">
            <a href="/">
                <i class="fas fa-check-circle fa-4x text-brand-yellow-1"></i>
            </a>
        </x-slot>
        <h3> Password updated </h3>
        <p> Congratulations! Your password has been successfully updated.</p>
        <div class="d-flex justify-content-center">
            <button class="btn btn-primary w-70 bg-brand-yellow-1 border-0 py-3 btn-rounded shadow-sm" onclick="window.location='{{ route('login') }}'">
                Login
            </button>
        </div>
    </x-password-card>
</x-guest-layout>