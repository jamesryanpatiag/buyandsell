<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <form method="POST" action="{{ route('login') }}">
            @csrf
            <!-- Session Status -->
            <div class="row">
                <div class="col">
                    <x-auth-session-status class="mb-4" :status="session('status')" />
                </div>
            </div>

            <!-- Validation Errors -->
            <div class="row">
                <div class="col">
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                </div>
            </div>

            <!-- Email Address -->
            <div class="row">
                <div class="col mb-3">
                    <x-label for="username" class="form-label" :value="__('Email')" />
                    <x-input id="username" class="" type="text" name="username" :value="old('username')" required autofocus />
                </div>
            </div>

            <!-- Password -->
            <div class="row">
                <div class="col mb-3">
                    <x-label for="password" class="form-label" :value="__('Password')" />
                    <x-input id="password" class=""
                                type="password"
                                name="password"
                                required autocomplete="current-password" />
                </div>
            </div>

            <!-- Remember Me -->
            <div class="row">
                <div class="col mb-3">
                    <label for="remember_me" class="d-inline-flex">
                        <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember">
                        <span class="ms-2 fs-6 text-gray-600">{{ __('Remember me') }}</span>
                    </label>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="d-flex justify-content-end">
                        @if (Route::has('password.request'))
                            <a class="btn btn-link fs-6 text-gray-600" href="{{ route('password.request') }}">
                                {{ __('Forgot your password?') }}
                            </a>
                        @endif
                        <x-button class="btn btn-primary ms-3">
                            {{ __('Log in') }}
                        </x-button>
                    </div>
                </div>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
