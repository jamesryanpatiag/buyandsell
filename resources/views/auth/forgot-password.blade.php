<x-guest-layout>
    <x-password-card>
        <div class="py-4">
            <x-slot name="logo">
                <a href="/">
                    <i class="fas fa-lock fa-4x text-brand-yellow-1"></i>
                </a>
            </x-slot>
            <h3> Forgot your password? </h3>
            <p class="px-14"> Enter your email address below to reset your password </p>
            <form method="POST" action="{{ route('password.email') }}">
                @csrf
                <!-- Session Status -->
                <div class="row">
                    <div class="col">
                        <x-auth-session-status class="mb-4" :status="session('status')" />
                    </div>
                </div>




                <!-- Email Address -->
                <div class="row d-flex justify-content-center">
                    <div class="col col-sm-8 mb-3">
                        <x-form.input id="email" class="py-3" placeholder="Email" name="email" :value="old('email')" autofocus />
                    </div>
                </div>
{{--                <!-- Validation Errors -->--}}
{{--                <div class="row">--}}
{{--                    <div class="col">--}}
{{--                        <x-forgot-password-errors class="mb-4" :errors="$errors" />--}}
{{--                    </div>--}}
{{--                </div>--}}

                    <div class="row">
                    <div class="col">
                        <div class="d-flex justify-content-center">
                            <x-button class="btn btn-primary w-70 bg-brand-yellow-1 border-0 py-3 btn-rounded shadow-sm">
                                {{ __('Reset Password') }}
                            </x-button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </x-password-card>
</x-guest-layout>
