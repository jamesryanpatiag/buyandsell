<div class="w-100">
    <div class="container">
        <div class="row">
            <div class="d-none d-lg-flex col-12 col-lg-4 col-xl-3 pe-xl-0 my-3 justify-content-start align-items-center">
                <img src="{{ asset('img/brand-logo.png') }}" width="38px" height="37.75px" class="ms-1 me-3" />
                <img src="{{ asset('img/brand-name.png') }}" style="max-height:37.5px;" class="img-fluid" />
            </div>
            <div class="col-10 col-sm-9 col-md-10 col-lg-6 col-xl-8 my-3 justify-content-center justify-content-md-start justify-content-lg-end d-flex">
                <form name="formSearch" class="row w-100">
                    @csrf
                    <div class="col-11 gx-0 gx-xl-5">
                        <div class=" input-group input-group-header-search">
                            <input type="text" class="form-control" name="search" placeholder="" aria-label="" aria-describedby="button-addon2">
                            <button class="btn btn-outline-secondary btn-header-search" type="button" id="button-addon2"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-2 col-sm-3 col-md-2 col-lg-2 col-xl-1 my-3 justify-content-end d-flex align-items-center">
                @if(\Auth::guard('member')->check())
                <a href="{{ route("product.create") }}" class="btn d-block d-md-inline px-2 px-sm-4 px-lg-5 bg-brand-yellow-1 text-white">Sell</a>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col">
                <hr class="mb-0">
            </div>
        </div>
    </div>
</div>
