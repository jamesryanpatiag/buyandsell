<div class="w-100">
    <nav class="navbar navbar-expand-lg navbar-dark bg-brand-gray-1" id="HeaderNav">
        <div class="container">
            <a class="navbar-brand" href="#"><img src="{{ asset('img/brand-logo-white.png') }}" width="33px" height="32.5px" class="" /></a>
            <div class="collapse navbar-collapse nav-links" >
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Buy Equipment</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route("product.index") }}">Sell Equipment</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Rent Equipment</a>
                    </li>
                </ul>
            </div>
            <div id="signup_cont" class="expand">
                <div class="d-flex justify-content-end me-3 me-xl-0 justify-content-xxl-center align-items-end align-items-xxl-start h-100 flex-column">
                    <div class="d-flex align-items-center flex-row position-relative">
                        @if(!\Auth::guard('member')->check())
                        <a class="text-white align-self-center text-decoration-none" href="{{ route('login') }}">Login</a>
                        <a class="ms-3 text-white align-self-center text-decoration-none" href="{{ route('register') }}">Sign Up</a>
                        @else
                            <a href="javascript:;" class="user-panel-toggle"><i class="fas fa-bars"></i></a>
                        @endif
                        <div class="user-panel desktop">
                            <a href="{{ route('profile.index') }}" class="btn d-block btn-link text-white align-self-center text-decoration-none">Profile</a>
                            <a href="{{ route('profile.edit') }}" class="btn d-block btn-link text-white align-self-center text-decoration-none">Settings</a>
                            <button type="button" class="btn btnLogout w-100 d-block btn-link text-white align-self-center text-decoration-none">Logout</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>
