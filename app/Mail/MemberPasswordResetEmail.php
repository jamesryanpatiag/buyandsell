<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MemberPasswordResetEmail extends Mailable
{
    use Queueable, SerializesModels;

    public string $resetUrl;
    public string $token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $token, CanResetPassword $user)
    {
        $this->token = $token;
        $this->resetUrl = url(route('password.reset', [
            'token' => $token,
            'email' => $user->getEmailForPasswordReset(),
        ], false));
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): MemberPasswordResetEmail
    {
        return $this->markdown('emails.auth.password-reset-notification');
    }
}
