<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ProductItem extends Component
{

    public $product;

    /**
     * Create a new component instance.
     *
     * @param $product
     * @param string|null $filter
     * @throws Exception
     */
    public function __construct($product, $filter=null)
    {
//        parent::__construct($product);
        $this->product = $product;
    }
    /**
     * Get the view / contents that represents the component.
     *
     * @return \Illuminate\View\View
     */
    public function render()
    {
        return view('components.product-item',['product'=>$this->product]);
    }
}
