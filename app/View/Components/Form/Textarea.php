<?php

namespace App\View\Components\Form;

use Closure;
use Illuminate\Contracts\View\View;

class Textarea extends BasicFormComponent
{
    /**
     * Get the view / contents that represent the component.
     *
     * @return View|Closure|string
     */
    public function render()
    {
        return view('components.form.textarea');
    }
}
