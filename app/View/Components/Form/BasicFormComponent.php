<?php

namespace App\View\Components\Form;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\View\Component;

abstract class BasicFormComponent extends Component
{
    static private array $form_elements = [];

    public string $id;
    public string $name;
    public string $label;
    public string $type;
    public ?string $value;
    public bool $required;
    public ?string $notes;
    public ?string $suffix;
    public ?string $prefix;
    public ?string $errorMessage;
    public ?string $successMessage;

    /**
     * Create a new component instance.
     *
     * @param $name
     * @param bool $required
     * @param null $value
     * @param null $id
     * @param null $type
     * @param null $label
     * @param null $notes
     * @param null $errorMessage
     * @param null $successMessage
     * @param mixed $prefix
     * @param mixed $suffix
     * @throws Exception
     */
    public function __construct($name, bool $required = true, $value = null, $id = null, $type = null, $label = null, $notes = null, $errorMessage = null, $successMessage = null, $prefix = false, $suffix = false)
    {
        $this->name = $name;
        $this->id = $id ?? $name;
        $this->label = $label ?? ucwords(str_replace(['-', '_'], ' ', $name));
        $this->type = $type ?? 'text';
        $this->value = old($name, $value instanceof Model ? $value->{$name} : $value);
        $this->required = $required;
        $this->notes = $notes;
        $this->prefix = $prefix;
        $this->suffix = $suffix;
        self::addElement($name);
        $this->errorMessage = $errorMessage;
        $this->successMessage = $successMessage;
    }

    /**
     * @throws Exception
     */
    public static function addElement($name)
    {
        if(in_array($name, self::$form_elements)) {
            throw new Exception("Duplicate element name added: " . $name);
        }
        self::$form_elements[] = $name;
    }

    public static function resetFormElementsList()
    {
        self::$form_elements = [];
    }
}
