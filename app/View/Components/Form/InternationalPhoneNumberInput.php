<?php

namespace App\View\Components\Form;

use Illuminate\Contracts\View\View;

class InternationalPhoneNumberInput extends Input
{
    /**
     * Get the view / contents that represent the component.
     *
     * @return View
     */
    public function render(): View
    {
        return view('components.form.international-phone-number-input');
    }
}
