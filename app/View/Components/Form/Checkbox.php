<?php

namespace App\View\Components\Form;

use Closure;
use Illuminate\Contracts\View\View;

class Checkbox extends BasicFormComponent
{
    public function __construct($name, $required = true, $value = null, $id = null, $type = null, $label = null)
    {
        parent::__construct($name, $required, $value, $id, $type, $label);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|Closure|string
     */
    public function render()
    {
        return view('components.form.checkbox');
    }
}
