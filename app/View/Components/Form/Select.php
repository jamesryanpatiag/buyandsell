<?php

namespace App\View\Components\Form;

use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Select extends BasicFormComponent
{
    public $optionsAreAssociative;
    public $options;
    public $optionsAreModels = false;
    public $filter;

    /**
     * Create a new component instance.
     *
     * @param $name
     * @param $options
     * @param bool $required
     * @param string|null $value
     * @param string|null $id
     * @param string|null $label
     * @param string|null $filter
     * @throws Exception
     */
    public function __construct($name, $options, $required = true, $value = null, $id = null, $label = null, $filter=null)
    {
        parent::__construct($name, $required, $value, $id, null, $label);
        $this->optionsAreAssociative = is_array($options) && Arr::isAssoc($options);
        if($options instanceof Collection ||
            ($this->optionsAreModels = is_array($options) && count($options) > 0 && reset($options) instanceof Model)
        ) {
            $this->optionsAreModels = true;
        }

        $this->options = $options ?? [];
        $this->filter = $filter;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return Application|Factory|View
     */
    public function render()
    {
        return view('components.form.select');
    }

    public function selected($key, $label): bool
    {
        if($this->optionsAreModels) {
            return $this->value == $label->id;
        } else if($this->optionsAreAssociative) {
            return $this->value == $key;
        } else {
            return $this->value == $label;
        }
    }
}
