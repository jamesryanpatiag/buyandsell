<?php

namespace App\View\Components\Form;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Model;

class MobileNumberInput extends Input
{
    public $prefix_id;
    public $prefix_name;
    public $prefix_value;

    public function __construct($name, $required = true, $value = null, $id = null, $type = null, $label = null, $notes = null, $prefix = null)
    {
        parent::__construct($name, $required, $value, $id, $type, $label, $notes);
        $this->prefix_id = $prefix ?:  $this->id . '_prefix';
        $this->prefix_name = $prefix ?: $this->name . '_prefix';
        if($value && $value instanceof Model) {
            $this->prefix_value = old($this->prefix_name, $value->{$this->prefix_name});
        } else {
            $this->prefix_value = old($this->prefix_name, $this->attributes ? $this->attributes->get('prefix_value') : null);
        }

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|Closure|string
     */
    public function render()
    {
        return view('components.form.mobile-number-input');
    }
}
