<?php
namespace App\Helpers;

class ListHelper
{
    public static function getMemberLocationAreas(): array
    {
        return array_keys(config('lists.member_location'));
    }

    public static function getMemberLocationRegions(?string $area): array
    {
        if(!empty($area) && key_exists($area, config('lists.member_location'))) {
            return array_keys(config('lists.member_location')[$area]);
        }
        return [];
    }

    public static function getMemberLocationProvinces(?string $area, ?string $region)
    {
        if(!empty($area) && !empty($region)) {
            $areas = config('lists.member_location');
            if(key_exists($area, $areas)) {
                $regions = $areas[$area];
                if(key_exists($region, $regions)) {
                    return $regions[$region];
                }
            }
        }
        return [];
    }
}