<?php

namespace App\Listeners;

use App\Models\Member;
use Illuminate\Auth\Events\Registered;

class SendMemberRegistrationSuccessEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        if ($event->user instanceof Member) {
            $event->user->sendMemberRegistrationSuccessEmail();
        }
    }
}
