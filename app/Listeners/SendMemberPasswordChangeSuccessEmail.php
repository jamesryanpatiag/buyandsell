<?php

namespace App\Listeners;

use App\Models\Member;
use Illuminate\Auth\Events\PasswordReset;

class SendMemberPasswordChangeSuccessEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PasswordReset  $event
     * @return void
     */
    public function handle(PasswordReset $event)
    {
        if ($event->user instanceof Member) {
            $event->user->sendMemberPasswordChangeSuccessEmail();
        }
    }
}
