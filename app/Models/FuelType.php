<?php

namespace App\Models;

use App\Models\Traits\SelectOptionsTrait;

/**
 * @method static selectOptions()
 */
class FuelType extends BasicModel
{
    use SelectOptionsTrait;

    protected $fillable = [
        'name',
    ];
}
