<?php /** @noinspection PhpHierarchyChecksInspection */

namespace App\Models;

use App\Http\Controllers\Member\GetMemberTrait;
use Exception;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection;

/**
 * @property int id
 * @property int main_image_id
 */
class Product extends BasicModel implements HasMedia
{
    use GetMemberTrait;
    use InteractsWithMedia;

    const YEAR_MIN = 1930;

    protected $fillable = [
        'title',
        'main_image_id',
        'description',
        'price',
        'rental',
        'rental_price_daily',
        'rental_price_weekly',
        'rental_price_monthly',
        'model',
        'brand',
        'year',
        'product_condition_id',
        'product_status_id',
        'equipment_mileage_id',
        'serial_number',
        'plate_number',
        'engine_make',
        'hours_of_use',
        'fuel_type_id',
        'front_tire_size',
        'rear_tire_size',
        'max_height',
        'max_width',
        'weight',
        'product_photo',
        'product_label',
        'youtube_url',
        'contact_first_name',
        'contact_last_name',
        'contact_address',
        'contact_location_province_id',
        'contact_number',
    ];

    /**
     * @throws FileIsTooBig
     * @throws FileDoesNotExist
     * @throws Exception
     */
    public function movePhotosFromMember(?array $media_ids, $labels = [])
    {
        $media_ids = is_array($media_ids) ? $media_ids : [];
        foreach($media_ids as $key => $media_id) {
            $media = $this->getMember()->getProductPhotos()->find($media_id);
            /** @noinspection PhpPossiblePolymorphicInvocationInspection */
            $media->setCustomProperty(Media::PROPERTY_DESCRIPTION, $labels[$key] ?? null);
            /** @noinspection PhpPossiblePolymorphicInvocationInspection */
            $media->move($this, Media::MEDIA_COLLECTION_PRODUCT_PHOTOS);
        }
    }

    public function getPhotos(): MediaCollection
    {
        return $this->getMedia(Media::MEDIA_COLLECTION_PRODUCT_PHOTOS);
    }

    public function member(): BelongsTo
    {
        return $this->belongsTo(Member::class);
    }

    public static function getYearRange(): array
    {
        return range(date('Y') + 1, self::YEAR_MIN);
    }

    public static function getIntegerRange(): string
    {
        return 'between:1,9999999';
    }

    public function product_package()
    {

    }

    /** @noinspection PhpUnused */
    public function main_image()
    {
        $ret = $this->getPhotos()->find($this->main_image_id);
        if($ret) {
            return $ret;
        } else {
            return $this->getPhotos()->first();
        }
    }

}
