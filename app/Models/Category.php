<?php /** @noinspection PhpHierarchyChecksInspection */

namespace App\Models;
use Illuminate\Support\Collection;

class Category extends BasicModel
{
    protected $fillable = [
        'name'
    ];
    public static function scopeSelectOptions(): Collection
    {
        return Category::select(['id', 'name'])
            ->orderBy('name', 'ASC')
            ->get();
    }
}
