<?php

namespace App\Models;

use App\Models\Traits\SelectOptionsTrait;

/**
 * @method static selectOptions()
 */
class ProductStatus extends BasicModel
{
    use SelectOptionsTrait;

    const KEY_AVAILABLE = 1;
    const KEY_NOT_AVAILABLE = 2;
    const KEY_SOLD = 3;

    protected $fillable = [
        'name',
    ];
}
