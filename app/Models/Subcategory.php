<?php

namespace App\Models;

use Spatie\Tags\HasTags;

class Subcategory extends BasicModel
{
    use HasTags;

    protected $fillable = [
        'name',
        'category_id',
    ];
}
