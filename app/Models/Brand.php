<?php

namespace App\Models;

use App\Models\Traits\SelectOptionsTrait;
use Illuminate\Support\Collection;

/**
 * @method static selectOptions()
 */
class Brand extends BasicModel
{
    protected $fillable = [
        'name',
        'is_top',
    ];


    public static function scopeSelectOptions(): Collection
    {
        return Brand::select(['id', 'name'])
            ->orderBy('is_top', 'DESC')
            ->orderBy('name', 'ASC')
            ->get();
    }
}
