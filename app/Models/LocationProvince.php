<?php

namespace App\Models;

use App\Models\Traits\SelectOptionsTrait;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int id
 *
 * @method static create(array $array)
 * @method static LocationProvince findOrFail($id)
 *
 * @property LocationRegion location_region
 *
 */
class LocationProvince extends BasicModel
{
    use SelectOptionsTrait;

    protected $fillable = [
        'name',
        'location_region_id',
    ];

    public function location_region(): BelongsTo
    {
        return $this->belongsTo(LocationRegion::class);
    }
}
