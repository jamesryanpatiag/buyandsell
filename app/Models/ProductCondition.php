<?php

namespace App\Models;

use App\Models\Traits\SelectOptionsTrait;

/**
 * @method static selectOptions()
 */
class ProductCondition extends BasicModel
{
    use SelectOptionsTrait;

    const KEY_NEW = 1;
    const KEY_USED = 2;

    protected $fillable = [
        'name',
    ];
}
