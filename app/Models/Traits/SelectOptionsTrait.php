<?php
namespace App\Models\Traits;

use Illuminate\Support\Collection;

/**
 * @method static select(string[] $array)
 */
trait SelectOptionsTrait
{
    public function scopeSelectOptions(): Collection
    {
        return self::select(['id', 'name'])->orderBy('name')->get();
    }
}