<?php

namespace App\Models\Traits;

use App\Models\Media;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Spatie\Image\Exceptions\InvalidManipulation;
use Spatie\Image\Image;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection;
use Str;

trait ManipulatesProductPhotosTrait
{
    use InteractsWithMedia;

    /**
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     * @throws InvalidManipulation
     */
    public function addProductPhotos(?array $files): array
    {
        $files = is_array($files) ? $files : [];
        $ret = [];
        foreach($files as $file) {
            if($file instanceof UploadedFile) {
                Image::load($file->getPathname())->width(1920)->height(1080)->optimize()->save();
                /** @var Media $media */
                $media = $this
                    ->addMedia($file)
                    ->withCustomProperties([
                        Media::PROPERTY_ORIGINAL_FILENAME => $file->getClientOriginalName(),
                    ])
                    ->usingFileName(Carbon::now()->format('Y-m-d.Hs') . '.' . Str::random(20) . '.' . $file->extension())
                    ->toMediaCollection(Media::MEDIA_COLLECTION_PRODUCT_PHOTOS);
                $ret[] = $media->id;
            }
        }
        return $ret;
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(Media::MEDIA_COLLECTION_PRODUCT_PHOTOS);
    }

    public function getProductPhotos(): MediaCollection
    {
        return $this->getMedia(Media::MEDIA_COLLECTION_PRODUCT_PHOTOS);
    }

    /** @noinspection PhpUnused */
    public function getProductPhotosProperties(array $ids): array
    {
        $media = $this->getProductPhotos()->whereIn('id', $ids);
        $ret = [];
        foreach($media as $medium) {
            /** @var Media $medium */
            $ret[] = $medium->getProductPhotoProperties();
        }
        return $ret;
    }

}