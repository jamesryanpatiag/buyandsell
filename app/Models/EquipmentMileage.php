<?php

namespace App\Models;

use App\Models\Traits\SelectOptionsTrait;
use Illuminate\Support\Collection;

/**
 *
 * @method static create(int[] $array)
 * @method static selectOptions()
 *
 * @property string name
 */
class EquipmentMileage extends BasicModel
{
    use SelectOptionsTrait;

    protected $fillable = [
        'min',
        'max',
    ];

    public function getNameAttribute(): string
    {
        return "{$this->min} - {$this->max}km";
    }

    public function scopeSelectOptions(): Collection
    {
        return self::select(['id', 'min', 'max'])
            ->orderBy('min')
            ->orderBy('max')
            ->get();
    }
}
