<?php /** @noinspection PhpHierarchyChecksInspection */

namespace App\Models;

use App\Mail\MemberPasswordChangeSuccessEmail;
use App\Mail\MemberRegistrationSuccessEmail;
use App\Mail\MemberPasswordResetEmail;
use App\Mail\MemberVerifyEmail;
use App\Models\Traits\ManipulatesProductPhotosTrait;
use Exception;
use File;
use Hash;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use Image;
use Log;
use Mail;
use Spatie\MediaLibrary\HasMedia;
use Storage;
use Str;

/**
 * @property string username
 * @property string email
 * @property string mobile_number
 * @property string password
 * @property string location_province_id
 * @property string first_name
 * @property string last_name
 * @property string company_name
 * @property string bio
 * @property string profile_photo
 * @property string email_verified_status
 *
 * @method static Member make(array $validated)
 * @method static Member forceCreate(array $validated)
 *
 * @property Collection|Product[] products
 */
class Member extends BasicModel implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract,
    \Illuminate\Contracts\Auth\MustVerifyEmail,
    HasMedia
{
    use Authenticatable, Authorizable, CanResetPassword, MustVerifyEmail, Notifiable;
    use ManipulatesProductPhotosTrait;

    protected $fillable = [
        'email',
        'mobile_number',
        'first_name',
        'last_name',
        'company_name',
        'bio',
        'location_province_id',
        'profile_photo',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function setEmailAttribute($value)
    {
        $new_email = trim(strtolower($value));
        if (isset($this->attributes['email']) && !empty($this->attributes['email']) && $new_email != ($this->attributes['email'] ?? null)) {
            $this->email_verified_at = null;
            $this->sendEmailVerificationNotification();
        }
        $this->attributes['email'] = $new_email;
    }

    /** @noinspection PhpUnused */
    public function getFullNameAttribute(): string
    {
        $names = [];
        if (!empty($this->first_name)) {
            $names[] = $this->first_name;
        }

        if (!empty($this->last_name)) {
            $names[] = $this->last_name;
        }

        if (count($names) == 0) {
            return 'User';
        }

        return implode(' ', $names);
    }

    /** @noinspection PhpUnused */
    public function setProfilePhotoAttribute($value)
    {
        if ($value ?? false) {
            $filename = $this->id . '-' . Str::random() . '.png';
            try {
                Image::make($value)->resize(300, 300)->save(Storage::disk('public')->path("profile_photos/$filename"));
            } catch (Exception $ignore) {
                File::ensureDirectoryExists(Storage::disk('public')->path("profile_photos"));
                Image::make($value)->resize(300, 300)->save(Storage::disk('public')->path("profile_photos/$filename"));
                Log::error("Member : setProfilePhotoAttribute : " . $ignore->getMessage() . " : " . $ignore->getTraceAsString());
            }
            $this->attributes['profile_photo'] = $filename;
        }
    }

    /** @noinspection PhpUnused */
    public function getProfilePhotoAttribute(): string
    {
        return !empty($this->attributes['profile_photo']) ? asset('storage/profile_photos/' . $this->attributes['profile_photo']) : 'https://via.placeholder.com/300?text=profile image';
    }

    /** @noinspection PhpUnused */
    public function getEmailVerifiedStatusAttribute(): string
    {
        return !empty($this->email_verified_at) ? 'Verified' : 'Not Verified';
    }

    public function sendMemberRegistrationSuccessEmail()
    {
        Mail::to($this->email)->send(new MemberRegistrationSuccessEmail);
    }

    public function sendMemberPasswordChangeSuccessEmail()
    {
        Mail::to($this->email)->send(new MemberPasswordChangeSuccessEmail());
    }

    public function sendPasswordResetNotification($token)
    {
        Mail::to($this->email)->send(new MemberPasswordResetEmail($token,$this));
    }

    public function sendEmailVerificationNotification()
    {
        $verificationUrl = URL::temporarySignedRoute(
            'verification.verify',
            Carbon::now()->addMinutes(Config::get('auth.verification.expire', 60)),
            [
                'id' => $this->getKey(),
                'hash' => sha1($this->getEmailForVerification()),
            ]
        );
        Mail::to($this->email)->send(new MemberVerifyEmail($verificationUrl,$this));
    }


    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }
}
