<?php

namespace App\Models;

use App\Models\Traits\SelectOptionsTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int id
 *
 * @method static create(array $array)
 * @method static LocationRegion findOrFail($id)
 *
 * @property Collection|LocationProvince[] location_provinces
 * @property LocationArea location_area
 */
class LocationRegion extends BasicModel
{
    use SelectOptionsTrait;

    protected $fillable = [
        'name',
        'location_area_id',
    ];

    /** @noinspection PhpUnused */
    public function location_provinces(): HasMany
    {
        return $this->hasMany(LocationProvince::class);
    }

    /** @noinspection PhpUnused */
    public function location_area(): BelongsTo
    {
        return $this->belongsTo(LocationArea::class);
    }
}
