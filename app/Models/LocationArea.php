<?php

namespace App\Models;

use App\Models\Traits\SelectOptionsTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int id
 *
 * @method static create(array $array)
 * @method static selectOptions()
 * @method static LocationArea findOrFail($id)
 *
 * @property Collection|LocationRegion[] location_regions
 *
 */
class LocationArea extends BasicModel
{
    use SelectOptionsTrait;

    protected $fillable = [
        'name',
    ];

    public function location_regions(): HasMany
    {
        return $this->hasMany(LocationRegion::class);
    }
}
