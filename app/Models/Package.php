<?php /** @noinspection PhpHierarchyChecksInspection */

namespace App\Models;

/**
 * @property int id
 * @property int days_valid
 * @property string duration
 * @property string name
 * @property int photo_limit
 * @property boolean is_featured
 * @property boolean is_premium
 * @property boolean can_add_video
 */
class Package extends BasicModel
{
    const KEY_BASIC = 1;
    const KEY_ENHANCED = 2;
    const KEY_PREMIUM = 3;

    protected $casts = [
        'is_featured' => 'boolean',
        'is_premium' => 'boolean',
    ];

    public function delete()
    {

    }

    /** @noinspection PhpUnused */
    public function getDurationAttribute(): string
    {
        if($this->days_valid % 7 == 0) {
            return $this->days_valid / 7 . ' WEEKS';
        }
        return $this->days_valid . ' DAYS';
    }

    /** @noinspection PhpUnused */
    public function getCanAddVideoAttribute(): bool
    {
        return true;
    }
}
