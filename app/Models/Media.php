<?php

namespace App\Models;

use Spatie\MediaLibrary\MediaCollections\Models\Media as BaseMedia;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * @property int id
 * @property string file_name
 */
class Media extends BaseMedia implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    const PROPERTY_DESCRIPTION = 'description';
    const PROPERTY_ORIGINAL_FILENAME = 'original-filename';
    const MEDIA_COLLECTION_PRODUCT_PHOTOS = 'product-photos';

    public function getDescription()
    {
        return $this->getCustomProperty(self::PROPERTY_DESCRIPTION, null);
    }

    public function getOriginalFilename()
    {
        return $this->getCustomProperty(self::PROPERTY_ORIGINAL_FILENAME, null);
    }

    public function getProductPhotoProperties(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->getOriginalFilename(),
            'url' => $this->getUrl(),
            'size' => $this->size,
            'description' => $this->getDescription(),
        ];
    }
}
