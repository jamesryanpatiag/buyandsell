<?php

namespace App\Rules;

use Illuminate\Validation\Rules;

trait MemberPasswordValidationRules
{
    public function memberPasswordRules(): array
    {
        return ['required', 'confirmed', Rules\Password::defaults()];
    }

}
