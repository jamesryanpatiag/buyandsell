<?php

namespace App\Rules;

use App\Http\Controllers\Member\GetMemberTrait;
use Exception;
use Illuminate\Validation\Rule;

trait MemberEmailValidationRules
{
    use GetMemberTrait;

    /**
     * @throws Exception
     */
    public function memberEmailRules($new = false): array
    {
        $ret = ['required', 'string', 'email', 'max:150'];
        if($new) {
            $ret[] = 'unique:members';
        } else {
            $ret[] = Rule::unique('members')->ignore($this->getMember()->id);
        }
        return $ret;
    }

}
