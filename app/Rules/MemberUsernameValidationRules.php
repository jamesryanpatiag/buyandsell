<?php

namespace App\Rules;

trait MemberUsernameValidationRules
{
    public function memberUsernameRules($new = false): array
    {
        $ret = ['required', 'string', 'alpha_dash', 'max:50', 'min:5'];
        if($new) {
            $ret[] = 'unique:members';
        }
        return $ret;
    }

}
