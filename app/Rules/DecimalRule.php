<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DecimalRule implements Rule
{
    const MAX = 999999999.99;
    private string $message;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $value = floatval($value);
        if($value < 0 || $value > self::MAX) {
            $this->message = "The :attribute field must be between 0 and " . self::MAX;
            return false;
        }
        if(($value * 100) != intval(($value * 100))) {
            $this->message = "The :attribute field must have up to 2 decimal places only";
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return $this->message;
    }
}
