<?php

namespace App\Rules;

use Exception;
use Illuminate\Contracts\Validation\Rule;
use Image;
use Log;

class ImageableRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        try {
            Image::make($value);
            return true;
        } catch (Exception $ignore) {
            Log::warning("Imageable : passes : " . $ignore->getMessage() . ' : ' . $ignore->getTraceAsString());
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be an image.';
    }
}
