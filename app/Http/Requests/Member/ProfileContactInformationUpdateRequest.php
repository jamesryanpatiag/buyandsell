<?php

namespace App\Http\Requests\Member;

use App\Rules\MemberEmailValidationRules;
use Exception;
use JetBrains\PhpStorm\ArrayShape;

class ProfileContactInformationUpdateRequest extends MemberOnlyRequest
{
    use MemberEmailValidationRules;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * @throws Exception
     */
    public function rules(): array
    {
        return [
            'mobile_number' => ['required', 'string', 'max:50'],
            'email' => $this->memberEmailRules(false),
        ];
    }
}
