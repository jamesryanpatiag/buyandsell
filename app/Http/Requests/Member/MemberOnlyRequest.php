<?php

namespace App\Http\Requests\Member;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class MemberOnlyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::guard('member')->check();
    }
}