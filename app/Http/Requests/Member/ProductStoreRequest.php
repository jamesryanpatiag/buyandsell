<?php

namespace App\Http\Requests\Member;

use App\Models\Package;
use App\Models\Product;
use App\Models\ProductCondition;
use App\Rules\DecimalRule;
use Illuminate\Validation\Rule;

class ProductStoreRequest extends MemberOnlyRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $package = Package::findOrFail(intval(request()->get('package_id')));
        $this->redirect = route('product.create', ['package_id' => $package->id]);
        return [
            'title' => ['required', 'string', 'max:100'],
            'description' => ['required', 'string', 'max:5000'],
            'price' => ['required', 'numeric', new DecimalRule()],

            'rental' => ['nullable', 'numeric', Rule::in(0,1)],
            'rental_price_daily' => ['required_if:rental,1', 'numeric','nullable', new DecimalRule()],
            'rental_price_weekly' => ['required_if:rental,1', 'numeric','nullable', new DecimalRule()],
            'rental_price_monthly' => ['required_if:rental,1', 'numeric','nullable', new DecimalRule()],

            'model' => ['required', 'string', 'max:50'],
            'brand' => ['required', 'string', 'max:50'],
//            'industry_category_id' => ['required', 'integer', Rule::exists('industry_categories', 'id')],
            'year' => ['required', 'integer', Rule::in(Product::getYearRange())],
//            'product_class_id' => ['required', 'integer', Rule::exists('product_classes', 'id')],
            'product_condition_id' => ['required', 'integer', Rule::exists('product_conditions', 'id')],
            'product_status_id' => ['required', 'integer', Rule::exists('product_statuses', 'id')],
            'equipment_mileage_id' => ['required', 'integer', Rule::exists('equipment_mileages', 'id')],

            'serial_number' => ['nullable', 'string', 'max:50'],
            'plate_number' => ['nullable', 'string', 'max:20'],
            'engine_make' => ['nullable', 'string', 'max:100'],
            'hours_of_use' => ['required_if:product_condition_id,'.ProductCondition::KEY_USED, 'nullable', 'integer', Product::getIntegerRange()],
            'fuel_type_id' => ['nullable', 'integer', Rule::exists('fuel_types', 'id')],
            'front_tire_size' => ['nullable', 'integer', Product::getIntegerRange()],
            'rear_tire_size' => ['nullable', 'integer', Product::getIntegerRange()],
            'max_height' => ['nullable', 'integer', Product::getIntegerRange()],
            'max_width' => ['nullable', 'integer', Product::getIntegerRange()],
            'weight' => ['nullable', 'integer', Product::getIntegerRange()],

            'product_photo' => ['required', 'array', 'max:' . $package->photo_limit],
            'product_label' => ['required', 'array', 'max:' . $package->photo_limit],
            'product_photo.*' => ['required', 'int'],
            'product_label.*' => ['nullable', 'string', 'max:100'],
            'youtube_url' => ['nullable', 'url', 'max:250'],


            'contact_first_name' => ['required', 'string', 'max:150'],
            'contact_last_name' => ['required', 'string', 'max:150'],
            'contact_address' => ['required', 'string', 'max:250'],
            'contact_location_province_id' => ['required', 'integer', Rule::exists('location_provinces', 'id')],
            'contact_number' => ['required', 'string', 'max:50'],
        ];
    }
}
