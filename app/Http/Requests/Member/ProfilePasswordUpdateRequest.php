<?php

namespace App\Http\Requests\Member;

use App\Rules\MemberPasswordValidationRules;

class ProfilePasswordUpdateRequest extends MemberOnlyRequest
{
    use MemberPasswordValidationRules;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'old_password' => ['required', 'string', 'current_password:member'],
            'new_password' => $this->memberPasswordRules(),
        ];
    }
}
