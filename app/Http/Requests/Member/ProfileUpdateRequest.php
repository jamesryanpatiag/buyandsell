<?php

namespace App\Http\Requests\Member;

use App\Helpers\ListHelper;
use App\Rules\ImageableRule;
use Illuminate\Validation\Rule;

class ProfileUpdateRequest extends MemberOnlyRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'first_name' => ['nullable', 'string', 'max:150'],
            'last_name' => ['nullable', 'string', 'max:150'],
            'company_name' => ['nullable', 'string', 'max:150'],
            'bio' => ['nullable', 'string', 'max:255'],
            'location_province_id' => ['required', 'int', Rule::exists('location_provinces', 'id')],
            'profile_photo' => ['nullable', new ImageableRule()],
        ];
    }
}
