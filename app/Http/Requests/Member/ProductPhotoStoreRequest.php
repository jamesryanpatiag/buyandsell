<?php

namespace App\Http\Requests\Member;

class ProductPhotoStoreRequest extends MemberOnlyRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'file.*' => ['required', 'file', 'image'],
            'product_photo.*' => ['nullable', 'integer'],
        ];
    }
}
