<?php

namespace App\Http\Requests\Auth;

use App\Helpers\ListHelper;
use App\Rules\MemberEmailValidationRules;
use App\Rules\MemberPasswordValidationRules;
use App\Rules\MemberUsernameValidationRules;
use Auth;
use Exception;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisteredUserStoreRequest extends FormRequest
{
    use MemberUsernameValidationRules, MemberEmailValidationRules, MemberPasswordValidationRules;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * @throws Exception
     */
    public function rules(): array
    {
        $this->redirect = route('register');
        return [
            'username' => $this->memberUsernameRules(true),
            'email' => $this->memberEmailRules(true),
            'password' => $this->memberPasswordRules(),
            'location_province_id' => ['required', 'int', Rule::exists('location_provinces', 'id')],
        ];
    }
}
