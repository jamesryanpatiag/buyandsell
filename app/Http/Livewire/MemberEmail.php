<?php

namespace App\Http\Livewire;

use App\Rules\MemberEmailValidationRules;
use Exception;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Validator;

class MemberEmail extends Component
{
    use MemberEmailValidationRules;

    public string $errorMessage = '';
    public string $successMessage = '';
    public string $email = '';
    public bool $new = true;

    public function render(): View
    {
        return view('livewire.member-email');
    }

    /**
     * @throws Exception
     */
    public function updatedEmail($newValue)
    {
        $newValue = trim($newValue);
        $data = ['email' => $newValue];
        $validator = Validator::make($data, [
            'email' => $this->memberEmailRules($this->new),
        ]);
        $this->errorMessage = $validator->errors()->get('email')[0] ?? '';

        if(empty($this->errorMessage) && !empty($newValue)) {
            $this->successMessage = 'Approved Email';
        } else {
            $this->successMessage = '';
        }
    }

    public function mount()
    {
        $this->email = old('email', $this->email);
    }

}
