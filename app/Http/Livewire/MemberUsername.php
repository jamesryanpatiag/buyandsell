<?php

namespace App\Http\Livewire;

use App\Rules\MemberUsernameValidationRules;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Validator;

class MemberUsername extends Component
{
    use MemberUsernameValidationRules;

    public string $errorMessage = '';
    public string $successMessage = '';
    public string $username = '';

    public function render(): View
    {
        return view('livewire.member-username');
    }

    public function updatedUsername($newValue)
    {
        $newValue = trim($newValue);
        $data = ['username' => $newValue];
        $validator = Validator::make($data, [
            'username' => $this->memberUsernameRules(true),
        ]);
        $this->errorMessage = $validator->errors()->get('username')[0] ?? '';

        if(empty($this->errorMessage) && !empty($newValue)) {
            $this->successMessage = 'Approved Username';
        } else {
            $this->successMessage = '';
        }
    }

    public function mount()
    {
        $this->username = old('username', $this->username);
    }
}
