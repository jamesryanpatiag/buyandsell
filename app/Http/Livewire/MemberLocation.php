<?php

namespace App\Http\Livewire;

use App\Models\LocationArea;
use App\Models\LocationProvince;
use App\Models\LocationRegion;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;
use Illuminate\Support\Facades\Route;

class MemberLocation extends Component
{
    public Collection $areas;
    public Collection $regions;
    public Collection $provinces;
    //
    public ?string $area = '';
    public ?string $region = '';
    public ?string $province = '';
    public string $currentRoute;
    public $prefix;
    public $value;


    public function __construct($id = null, $prefix = '', $value = null)
    {
        parent::__construct($id);
        $this->areas = LocationArea::selectOptions();
        $this->regions = Collection::empty();
        $this->provinces = Collection::empty();
        $this->prefix = $prefix;
        $this->value = $value;


    }

    public function render(): View
    {
        return view('livewire.member-location');
    }

    /** @noinspection PhpUnused */
    public function updatedArea($newValue)
    {
        if(!empty($newValue)) {
            $this->regions = LocationArea::findOrFail($newValue)->location_regions;
            if(!$this->regions->find($this->region)) {
                $this->region = '';
                $this->province = '';
                $this->provinces = Collection::empty();
            }
        }
    }

    public function updatedRegion($newValue)
    {
        if(!empty($newValue)) {
            $this->provinces = LocationRegion::findOrFail($newValue)->location_provinces;
            if(!$this->provinces->find($this->province)) {
                $this->province = '';
            }
        }
    }

    public function mount()
    {
        if($this->value != null) {
            $location_province = LocationProvince::findOrFail($this->value);
            $this->province = $location_province->id;
            $this->region = $location_province->location_region->id;
            $this->area = $location_province->location_region->location_area->id;
        }

        $this->area = old($this->prefix.'location_area_id', $this->area) ?? '';
        $this->region = old($this->prefix.'location_region_id', $this->region) ?? '';
        $this->province = old($this->prefix.'location_province_id', $this->province) ?? '';

        $this->currentRoute = Route::currentRouteName();
        $this->updatedArea($this->area);
        $this->updatedRegion($this->region);

    }

}
