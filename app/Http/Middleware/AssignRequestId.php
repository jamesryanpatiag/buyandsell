<?php

namespace App\Http\Middleware;

use App\Models\Member;
use Auth;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class AssignRequestId
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @noinspection PhpMissingReturnTypeInspection
     */
    public function handle(Request $request, Closure $next)
    {
        $requestId = (string) Str::uuid();

        Log::withContext([
            'request-id' => $requestId
        ]);

        if(in_array(strtolower($request->method()), ['post', 'put', 'patch', 'delete'])) {
            if(Auth::guard('member')->check()) {
                $user = Auth::guard('member')->user();
                if($user instanceof Member) {
                    Log::info("Member Logged In: " . $user->id);
                }
            }
        }

        return $next($request);
    }
}