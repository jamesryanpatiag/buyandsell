<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\ViewErrorBag;
use Log;
use Route;

class FormValidationErrorLoggingMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return Response
     */
    public function handle(Request $request, Closure $next)
    {

        try {
            /**
             * @var ViewErrorBag $messageBag
             */
            $messageBag = session()->get('errors');
            if($messageBag instanceof ViewErrorBag && $messageBag->isNotEmpty()) {
                Log::warning(Route::currentRouteAction() . " Validation ErrorBag  : " . json_encode($messageBag->toArray()));
            }
        } catch (Exception $e) {
            Log::error(Route::currentRouteAction() ." Validation ErrorBag check error : " . $e->getMessage());
        }

        return $next($request);
    }
}
