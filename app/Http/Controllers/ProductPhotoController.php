<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Member\MemberOnlyController;
use App\Http\Requests\Member\ProductPhotoStoreRequest;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;

class ProductPhotoController extends MemberOnlyController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param ProductPhotoStoreRequest $request
     * @return JsonResponse
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     * @throws Exception
     */
    public function store(ProductPhotoStoreRequest $request): JsonResponse
    {
        $data = $request->validated();
        $media_to_delete = $this->getMember()->getProductPhotos()->where('created_at', '<', Carbon::now()->addHours(-1)->toDateTimeString())->whereNotIn('id', $data['product_photo'] ?? []);
        foreach( $media_to_delete ?? [] as $medium_to_delete) {
            $medium_to_delete->delete();
        }
        $media = $this->getMember()->addProductPhotos($data['file'] ?? null);
        return response()->json([
            'success' => true,
            'data' => $media,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(int $id): JsonResponse
    {
        $this->getMember()->getProductPhotos()->find($id)->delete();
        return response()->json([
            'success' => true,
        ]);
    }
}
