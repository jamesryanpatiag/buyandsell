<?php
namespace App\Http\Controllers\Member;

use App\Models\Member;
use Auth;
use Exception;

trait GetMemberTrait
{
    private Member $member;
    /**
     * @throws Exception
     * @return Member
     */
    protected function getMember(): Member
    {
        $user = null;
        if (!empty($this->member)) {
            $this->member->refresh();
            $user = $this->member;
        } else {
            $user = Auth::guard('member')->user();
        }
        if ($user instanceof Member) {
            $this->member = $user;
            return $this->member;
        }
        throw new Exception('Error retrieving logged in member: '. $this->member);
    }
}