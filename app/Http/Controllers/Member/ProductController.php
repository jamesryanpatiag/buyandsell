<?php

namespace App\Http\Controllers\Member;

use App\Http\Requests\Member\ProductStoreRequest;
use App\Models\Industry;
use App\Models\Subcategory;
use App\Models\Package;
use App\Models\Product;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductController extends MemberOnlyController
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view('product.package', [
            'packages' => Package::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     * @throws Exception
     */
    public function create(): View
    {
        $package = Package::findOrFail(intval(request()->get('package_id')));
        return view('product.create',[
            'profile' => $this->getMember(),
            'package' => $package,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductStoreRequest $request
     * @return RedirectResponse
     * @throws Exception
     */
    public function store(ProductStoreRequest $request): RedirectResponse
    {
        $data = $request->validated();
        $full_data = $data;
        unset($data['product_photo']);
        unset($data['product_label']);
        /** @var Product $product */
        $product = $this->getMember()->products()->create($data);
        $product->movePhotosFromMember($full_data['product_photo'], $full_data['product_label'] ?? []);
        /** @noinspection PhpPossiblePolymorphicInvocationInspection */
        return redirect()-> route('product.edit', [
            'product' => $this->getMember()->products()->findOrFail($product->id)->id,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return view('product.edit',[
            'profile' => $this->getMember(),
            'product' => $this->getMember()->products()->findOrFail($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function getSubCategories(Request $request)
    {
        $categoryId = intval($request->route('categoryId'));
        return Subcategory::where('category_id','=', $categoryId)->get();
    }

}
