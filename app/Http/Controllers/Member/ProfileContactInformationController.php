<?php

namespace App\Http\Controllers\Member;

use App\Http\Requests\Member\ProfileContactInformationUpdateRequest;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ProfileContactInformationController extends MemberOnlyController
{
    /**
     * Display a listing of the resource.
     *
     * @throws Exception
     */
    public function edit(): View
    {
        return view('profile.contact_information.edit', [
            'profile' => $this->getMember(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProfileContactInformationUpdateRequest $request
     * @return RedirectResponse
     * @throws Exception
     */
    public function update(ProfileContactInformationUpdateRequest $request): RedirectResponse
    {
        $this->getMember()->update($request->validated());
        return redirect()->route('profile.contact-information.edit')->with(['message' => 'Successfully updated contact information.']);
    }

}