<?php
namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Exception;

class MemberOnlyController extends Controller
{
    use GetMemberTrait;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $this->middleware('auth:member');


    }



}