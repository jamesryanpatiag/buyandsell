<?php

namespace App\Http\Controllers\Member;

use App\Http\Requests\Member\ProfilePasswordUpdateRequest;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ProfilePasswordController extends MemberOnlyController
{
    /**
     * Display a listing of the resource.
     *
     * @throws Exception
     */
    public function edit(): View
    {
        return view('profile.password.edit', [
            'profile' => $this->getMember(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProfilePasswordUpdateRequest $request
     * @return RedirectResponse
     * @throws Exception
     */
    public function update(ProfilePasswordUpdateRequest $request): RedirectResponse
    {
        $member = $this->getMember();
        $member->password = $request->validated()['new_password'];
        $member->save();
        $member->sendMemberPasswordChangeSuccessEmail();
        return redirect()->route('profile.password.edit')->with(['message' => 'Successfully updated password.']);
    }

}