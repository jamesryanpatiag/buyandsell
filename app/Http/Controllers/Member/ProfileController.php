<?php

namespace App\Http\Controllers\Member;

use App\Http\Requests\Member\ProfileUpdateRequest;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ProfileController extends MemberOnlyController
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     * @throws Exception
     */
    public function index(): View
    {
        return view('profile.index', [
            'profile' => $this->getMember(),
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @return View
     * @throws Exception
     */
    public function edit(): View
    {
        return view('profile.edit', [
            'profile' => $this->getMember()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProfileUpdateRequest $request
     * @return RedirectResponse
     * @throws Exception
     */
    public function update(ProfileUpdateRequest $request): RedirectResponse
    {
        $this->getMember()->update($request->validated());
        return redirect()->route('profile.edit')->with(['message' => 'Update success.']);
    }

}