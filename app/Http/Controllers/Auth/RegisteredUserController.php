<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisteredUserStoreRequest;
use App\Models\Member;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return View
     */
    public function create(): View
    {
        return view('auth._login-registration', [
            'is_registration' => true
        ]);
    }

    /**
     * Handle an incoming registration request.
     *
     * @param RegisteredUserStoreRequest $request
     * @return RedirectResponse
     *
     */
    public function store(RegisteredUserStoreRequest $request): RedirectResponse
    {
        $user = Member::forceCreate($request->validated());

        event(new Registered($user));

        Auth::login($user);

        return redirect(RouteServiceProvider::HOME);
    }
}
