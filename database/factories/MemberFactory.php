<?php

namespace Database\Factories;

use App\Helpers\ListHelper;
use App\Models\LocationProvince;
use App\Models\Member;
use Arr;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class MemberFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Member::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {

        return [
            'username' => $this->faker->regexify('[A-Za-z0-9]{50}'),
            'email' => $this->faker->unique->email,
            'password' => $this->faker->password,
            'location_province_id' => LocationProvince::inRandomOrder()->first()->id,
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'company_name' => $this->faker->company,
            'bio' => $this->faker->text(255),
            'email_verified_at' => Carbon::yesterday()->toDateTimeString(),
            'mobile_number' => $this->faker->phoneNumber,
        ];
    }
}
