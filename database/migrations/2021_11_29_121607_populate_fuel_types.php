<?php

use App\Models\FuelType;
use Illuminate\Database\Migrations\Migration;

class PopulateFuelTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        FuelType::forceCreate(['id' => 1, 'name' => 'BioDiesel']);
        FuelType::forceCreate(['id' => 2, 'name' => 'Diesel']);
        FuelType::forceCreate(['id' => 3, 'name' => 'Gas']);
        FuelType::forceCreate(['id' => 4, 'name' => 'Hybrid']);
        FuelType::forceCreate(['id' => 5, 'name' => 'LPG']);
        FuelType::forceCreate(['id' => 6, 'name' => 'None']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
