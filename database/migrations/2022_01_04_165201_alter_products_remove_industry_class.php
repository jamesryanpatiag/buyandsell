<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterProductsRemoveIndustryClass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('app.env') != 'testing') {
            Schema::table('products', function (Blueprint $table) {
                if($this->keyExists('products_industry_category_id_foreign')){
                    $table->dropForeign('products_industry_category_id_foreign');
                }
                if(Schema::hasColumn('products', 'industry_category_id')) {
                    $table->dropColumn('industry_category_id');
                }
                if($this->keyExists('products_product_class_id_foreign')){
                    $table->dropForeign('products_product_class_id_foreign');
                }
                if(Schema::hasColumn('products', 'product_class_id')) {
                    $table->dropColumn('product_class_id');
                }


            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }

    private function keyExists($key)
    {
        return DB::select(
            DB::raw(
                'SHOW KEYS
        FROM products
        WHERE Key_name=\''.$key.'\''
            )
        );
}
}
