<?php

use App\Models\EquipmentMileage;
use Illuminate\Database\Migrations\Migration;

class PopulateEquipmentMileages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        EquipmentMileage::create(['min' => 0, 'max'=> 10000]);
        EquipmentMileage::create(['min' => 10001, 'max'=> 20000]);
        EquipmentMileage::create(['min' => 20001, 'max'=> 30000]);
        EquipmentMileage::create(['min' => 30001, 'max'=> 40000]);
        EquipmentMileage::create(['min' => 40001, 'max'=> 50000]);
        EquipmentMileage::create(['min' => 50001, 'max'=> 60000]);
        EquipmentMileage::create(['min' => 60001, 'max'=> 70000]);
        EquipmentMileage::create(['min' => 70001, 'max'=> 80000]);
        EquipmentMileage::create(['min' => 80001, 'max'=> 90000]);
        EquipmentMileage::create(['min' => 90001, 'max'=> 100000]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
