<?php

use App\Models\ProductCondition;
use Illuminate\Database\Migrations\Migration;

class PopulateProductCondition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ProductCondition::forceCreate(['id' => ProductCondition::KEY_NEW, 'name' => 'New']);
        ProductCondition::forceCreate(['id' => ProductCondition::KEY_USED, 'name' => 'Used']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
