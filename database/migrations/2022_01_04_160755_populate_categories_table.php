<?php

use App\Models\Category;
use Illuminate\Database\Migrations\Migration;

class PopulateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Category::create(['name' => 'Aggregate Equipment']);
        Category::create(['name' => 'Aggregate Handling Equipment']);
        Category::create(['name' => 'Agricultural Attachments']);
        Category::create(['name' => 'Agricultural Tractors']);
        Category::create(['name' => 'Air Compressor and Treatment Equipment']);
        Category::create(['name' => 'Air Drills and Seeders']);
        Category::create(['name' => 'Articulated Dump Truck']);
        Category::create(['name' => 'Asphalt Compactor']);
        Category::create(['name' => 'Asphalt Paver']);
        Category::create(['name' => 'Asphalt Paving Equipment']);
        Category::create(['name' => 'Asphalt Plant']);
        Category::create(['name' => 'Asphalt Plant Components']);
        Category::create(['name' => 'Asphalt Trailers']);
        Category::create(['name' => 'Asphalt Trucks']);
        Category::create(['name' => 'Backhoe Loader']);
        Category::create(['name' => 'Backhoe Loader Attachments']);
        Category::create(['name' => 'Balers and Hay Equipment']);
        Category::create(['name' => 'Bed and Rig Up Trucks']);
        Category::create(['name' => 'Beverage Trailers']);
        Category::create(['name' => 'Beverage Trucks']);
        Category::create(['name' => 'Boom Lift']);
        Category::create(['name' => 'Boom Trucks']);
        Category::create(['name' => 'Boring Equipment']);
        Category::create(['name' => 'Boring Machine']);
        Category::create(['name' => 'Bulldozer']);
        Category::create(['name' => 'Bulldozer Attachments']);
        Category::create(['name' => 'Bus']);
        Category::create(['name' => 'Cab and Chassis Trucks']);
        Category::create(['name' => 'Car Carrier Trailers']);
        Category::create(['name' => 'Car Carrier Trucks']);
        Category::create(['name' => 'Cargo Trucks']);
        Category::create(['name' => 'Chip Trailer']);
        Category::create(['name' => 'Chipping and Mulching Equipment']);
        Category::create(['name' => 'Clearing Equipment']);
        Category::create(['name' => 'Cold Planers']);
        Category::create(['name' => 'Combine Headers']);
        Category::create(['name' => 'Compactors']);
        Category::create(['name' => 'Concrete Equipment']);
        Category::create(['name' => 'Concrete Paving Equipment']);
        Category::create(['name' => 'Concrete Plant Components']);
        Category::create(['name' => 'Concrete Plants']);
        Category::create(['name' => 'Concrete Pumps']);
        Category::create(['name' => 'Concrete Truck Mounted Pump']);
        Category::create(['name' => 'Construction and Machinery Trailer']);
        Category::create(['name' => 'Construction Material']);
        Category::create(['name' => 'Conveyors']);
        Category::create(['name' => 'Crane Attachments']);
        Category::create(['name' => 'Crawler Cranes']);
        Category::create(['name' => 'Crawler Loaders']);
        Category::create(['name' => 'Crushing Equipment']);
        Category::create(['name' => 'Curtain Side Trailers']);
        Category::create(['name' => 'Demolition and Recycling Attachments']);
        Category::create(['name' => 'Digger Derrick Trucks']);
        Category::create(['name' => 'Directional Drills']);
        Category::create(['name' => 'Dollies']);
        Category::create(['name' => 'Drill Trucks']);
        Category::create(['name' => 'Drilling Equipment']);
        Category::create(['name' => 'Drilling Rigs']);
        Category::create(['name' => 'Dump Trailers']);
        Category::create(['name' => 'Dump Trucks']);
        Category::create(['name' => 'Electric Motors']);
        Category::create(['name' => 'Electrical Distribution Equipment']);
        Category::create(['name' => 'Emergency Vehicles']);
        Category::create(['name' => 'Environmental Equipment']);
        Category::create(['name' => 'Excavator']);
        Category::create(['name' => 'Excavator Attachments']);
        Category::create(['name' => 'Feed Handling Equipment']);
        Category::create(['name' => 'Feeders']);
        Category::create(['name' => 'Feller Bunchers']);
        Category::create(['name' => 'Fertilizer Equipment']);
        Category::create(['name' => 'Flat Bed and Cargo Trucks']);
        Category::create(['name' => 'Flatbed Trailers']);
        Category::create(['name' => 'Forklifts']);
        Category::create(['name' => 'Foundation/Piling Drills']);
        Category::create(['name' => 'Frac Tanks']);
        Category::create(['name' => 'Fracturing']);
        Category::create(['name' => 'Fuel and Lube Trailers']);
        Category::create(['name' => 'Fuel and Product Tanks']);
        Category::create(['name' => 'Fusion Machines']);
        Category::create(['name' => 'General Attachments']);
        Category::create(['name' => 'Generator Sets']);
        Category::create(['name' => 'Golf Course Maintenance Equipment']);
        Category::create(['name' => 'Grain Bins']);
        Category::create(['name' => 'Grain Carts']);
        Category::create(['name' => 'Grain Handling Equipment']);
        Category::create(['name' => 'Grain, Silage and Produce Trailers']);
        Category::create(['name' => 'Grain, Silage and Produce Trucks']);
        Category::create(['name' => 'Grout Pumps']);
        Category::create(['name' => 'Harrows and Packers']);
        Category::create(['name' => 'Harvesting Equipment']);
        Category::create(['name' => 'Heating and Cooling Equipment']);
        Category::create(['name' => 'Hook Trucks']);
        Category::create(['name' => 'Hydro Excavation Trucks']);
        Category::create(['name' => 'Irrigation Equipment']);
        Category::create(['name' => 'Landscape Loaders']);
        Category::create(['name' => 'Landscaping Equipment']);
        Category::create(['name' => 'Lift Truck Attachments']);
        Category::create(['name' => 'Light Towers']);
        Category::create(['name' => 'Live Bottom Trailers']);
        Category::create(['name' => 'Livestock Equipment']);
        Category::create(['name' => 'Livestock Trailers']);
        Category::create(['name' => 'Loader']);
        Category::create(['name' => 'Log Loaders']);
        Category::create(['name' => 'Log Trailers']);
        Category::create(['name' => 'Logging Equipment']);
        Category::create(['name' => 'Logging Trucks']);
        Category::create(['name' => 'Manlift Truck']);
        Category::create(['name' => 'Manufacturing Equipment']);
        Category::create(['name' => 'Marine Equipment']);
        Category::create(['name' => 'Material Handlers']);
        Category::create(['name' => 'Mining Equipment']);
        Category::create(['name' => 'Mixer Trucks']);
        Category::create(['name' => 'Mobile Crane']);
        Category::create(['name' => 'Modular Trailers']);
        Category::create(['name' => 'Motor Grader']);
        Category::create(['name' => 'Motor Grader Attachments']);
        Category::create(['name' => 'Motor Vehicles']);
        Category::create(['name' => 'Motorcycles']);
        Category::create(['name' => 'Mowers']);
        Category::create(['name' => 'Off Highway Trucks']);
        Category::create(['name' => 'Office Furniture']);
        Category::create(['name' => 'Oil and Gas Trailers']);
        Category::create(['name' => 'Oil and Gas Trucks']);
        Category::create(['name' => 'Order Picker']);
        Category::create(['name' => 'Other Agricultural Equipment']);
        Category::create(['name' => 'Other Trailers']);
        Category::create(['name' => 'Overhead Cranes']);
        Category::create(['name' => 'Pallet Jacks']);
        Category::create(['name' => 'Parts']);
        Category::create(['name' => 'Pile Hammers and Extractors']);
        Category::create(['name' => 'Pipelayers']);
        Category::create(['name' => 'Pipeline Equipment']);
        Category::create(['name' => 'Pneumatic Bulk Trailer']);
        Category::create(['name' => 'Pole Trailers']);
        Category::create(['name' => 'Portable Structures']);
        Category::create(['name' => 'Power Trowels']);
        Category::create(['name' => 'Precision Farming']);
        Category::create(['name' => 'Prime Movers']);
        Category::create(['name' => 'Production Equipment']);
        Category::create(['name' => 'Rail Equipment']);
        Category::create(['name' => 'Reach Truck']);
        Category::create(['name' => 'Road and Construction Broom']);
        Category::create(['name' => 'Road Wideners']);
        Category::create(['name' => 'Rough Terrain Forklifts']);
        Category::create(['name' => 'Sawmill Equipment']);
        Category::create(['name' => 'Scales']);
        Category::create(['name' => 'Scissor Lifts']);
        Category::create(['name' => 'Scrapers']);
        Category::create(['name' => 'Screening Equipment']);
        Category::create(['name' => 'Service and Utility Trucks']);
        Category::create(['name' => 'Silage and Forage Equipment']);
        Category::create(['name' => 'Site Dumpers']);
        Category::create(['name' => 'Skid Steer Attachments']);
        Category::create(['name' => 'Skid Steer Loaders']);
        Category::create(['name' => 'Skidders']);
        Category::create(['name' => 'Snow Equipment']);
        Category::create(['name' => 'Soil Stabilizers and Reclaimers']);
        Category::create(['name' => 'Sprayers']);
        Category::create(['name' => 'Stacker']);
        Category::create(['name' => 'Storage Containers']);
        Category::create(['name' => 'Surface Drill Rigs']);
        Category::create(['name' => 'Survey Equipment and Grade Control']);
        Category::create(['name' => 'Sweepers and Scrubbers']);
        Category::create(['name' => 'Tanker Trailers']);
        Category::create(['name' => 'Tanker Trucks']);
        Category::create(['name' => 'Telehandlers']);
        Category::create(['name' => 'Tillage Equipment']);
        Category::create(['name' => 'Tools']);
        Category::create(['name' => 'Tow Tractor']);
        Category::create(['name' => 'Towable Lifts']);
        Category::create(['name' => 'Tower Cranes']);
        Category::create(['name' => 'Traffic Control Devices']);
        Category::create(['name' => 'Trench Shoring']);
        Category::create(['name' => 'Trenchers']);
        Category::create(['name' => 'Tubular Handling Tools']);
        Category::create(['name' => 'Vacuum Systems']);
        Category::create(['name' => 'Vacuum Tanker Trucks']);
        Category::create(['name' => 'Vertical Mast Lifts']);
        Category::create(['name' => 'Very Narrow Aisle Truck']);
        Category::create(['name' => 'Waste and Recycling Equipment']);
        Category::create(['name' => 'Water Pumps']);
        Category::create(['name' => 'Water Related Equipment']);
        Category::create(['name' => 'Welding Machines and Equipment']);
        Category::create(['name' => 'Well Service']);
        Category::create(['name' => 'Wheel Loader Attachments']);
        Category::create(['name' => 'Wheel Loaders']);
        Category::create(['name' => 'Winch Trucks']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
