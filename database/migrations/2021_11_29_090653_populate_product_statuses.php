<?php

use App\Models\ProductStatus;
use Illuminate\Database\Migrations\Migration;

class PopulateProductStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ProductStatus::forceCreate(['id' => ProductStatus::KEY_AVAILABLE, 'name' => 'Available']);
        ProductStatus::forceCreate(['id' => ProductStatus::KEY_NOT_AVAILABLE, 'name' => 'Not Available']);
        ProductStatus::forceCreate(['id' => ProductStatus::KEY_SOLD, 'name' => 'Sold']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
