<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();


            $table->unsignedBigInteger('member_id')->index();
            $table->foreign('member_id')->references('id')->on('members');


            $table->string('title', 100);
            $table->text('description');
            $table->decimal('price', 15);
            $table->tinyInteger('rental')->default(0);
            $table->decimal('rental_price_daily')->nullable();
            $table->decimal('rental_price_weekly')->nullable();
            $table->decimal('rental_price_monthly')->nullable();
            $table->string('model', 50);
            $table->string('brand', 50);
            $table->unsignedInteger('year');

            $table->unsignedBigInteger('product_condition_id')->index();
            $table->foreign('product_condition_id')->references('id')->on('product_conditions');
            $table->unsignedBigInteger('product_status_id')->index();
            $table->foreign('product_status_id')->references('id')->on('product_statuses');
            $table->unsignedBigInteger('equipment_mileage_id')->index();
            $table->foreign('equipment_mileage_id')->references('id')->on('equipment_mileages');


            $table->string('serial_number', 50)->nullable();
            $table->string('plate_number', 20)->nullable();
            $table->string('engine_make', 100)->nullable();
            $table->unsignedInteger('hours_of_use')->nullable();
            $table->unsignedBigInteger('fuel_type_id')->nullable()->index();
            $table->foreign('fuel_type_id')->references('id')->on('fuel_types');
            $table->unsignedInteger('front_tire_size')->nullable();
            $table->unsignedInteger('rear_tire_size')->nullable();
            $table->unsignedInteger('max_height')->nullable();
            $table->unsignedInteger('max_width')->nullable();
            $table->unsignedInteger('weight')->nullable();

            $table->string('youtube_url', 250)->nullable();

            $table->string('contact_first_name', 150);
            $table->string('contact_last_name', 150);
            $table->string('contact_address', 250);
            $table->unsignedBigInteger('contact_location_province_id')->index();
            $table->foreign('contact_location_province_id')->references('id')->on('location_provinces');
            $table->string('contact_number', 50);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
