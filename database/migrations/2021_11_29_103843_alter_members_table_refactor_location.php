<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMembersTableRefactorLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('members', function (Blueprint $table) {
            if(config('app.env') != 'testing') {
                $table->unsignedBigInteger('location_province_id')->after('province')->default(1)->index();
                $table->foreign('location_province_id')->references('id')->on('location_provinces');
            }
            $table->dropColumn(['province', 'region', 'area']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('members', function (Blueprint $table) {
            //
        });
    }
}
