<?php

use App\Models\LocationArea;
use App\Models\LocationProvince;
use App\Models\LocationRegion;
use Illuminate\Database\Migrations\Migration;

class PopulateLocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $locations = config('lists.member_location');
        foreach($locations as $area => $regions) {
            $a = LocationArea::create(['name' => $area]);
            foreach($regions as $region => $provinces) {
                $r = LocationRegion::create(['location_area_id' => $a->id, 'name' => $region]);
                foreach($provinces as $province) {
                    LocationProvince::create(['location_region_id' => $r->id, 'name' => $province]);
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
