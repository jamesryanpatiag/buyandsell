<?php

use App\Models\Package;
use Illuminate\Database\Migrations\Migration;

class PopulatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Package::forceCreate([
            'id' => Package::KEY_BASIC,
            'name' => 'BASIC',
            'days_valid' => 14,
            'photo_limit' => 4,
            'is_featured' => false,
            'is_premium' => false,
            'price' => 1250,
        ]);
        Package::forceCreate([
            'id' => Package::KEY_ENHANCED,
            'name' => 'ENHANCED',
            'days_valid' => 8*7,
            'photo_limit' => 20,
            'is_featured' => true,
            'is_premium' => false,
            'price' => 2500,
        ]);
        Package::forceCreate([
            'id' => Package::KEY_PREMIUM,
            'name' => 'PREMIUM',
            'days_valid' => 36*7,
            'photo_limit' => 50,
            'is_featured' => true,
            'is_premium' => true,
            'price' => 4000,
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
