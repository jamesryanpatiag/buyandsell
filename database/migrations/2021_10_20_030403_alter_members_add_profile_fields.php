<?php /** @noinspection PhpUnused */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMembersAddProfileFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('members', function (Blueprint $table) {
            $table->string('profile_photo', 150)->nullable()->after('email');
            $table->string('bio', 255)->nullable()->after('email');
            $table->string('company_name', 150)->nullable()->after('email');
            $table->string('last_name', 150)->nullable()->after('email');
            $table->string('first_name', 150)->nullable()->after('email');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('members', function (Blueprint $table) {
            //
        });
    }
}
