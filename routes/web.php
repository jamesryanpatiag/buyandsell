<?php

use App\Http\Controllers\Member\ProductController;
use App\Http\Controllers\Member\ProfileContactInformationController;
use App\Http\Controllers\Member\ProfileController;
use App\Http\Controllers\Member\ProfilePasswordController;
use App\Http\Controllers\ProductPhotoController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth', 'verified'])->group(function () {
    Route::prefix('profile')->group(function () {
        Route::get('', [ProfileController::class, 'index'])->name('profile.index');
        Route::get('edit', [ProfileController::class, 'edit'])->name('profile.edit');
        Route::patch('', [ProfileController::class, 'update'])->name('profile.update');

        Route::get('help', function () {
            return view('profile/help');
        });
        Route::get('password/edit', [ProfilePasswordController::class, 'edit'])->name('profile.password.edit');
        Route::patch('password', [ProfilePasswordController::class, 'update'])->name('profile.password.update');

        Route::get('contact-information/edit', [ProfileContactInformationController::class, 'edit'])->name('profile.contact-information.edit');
        Route::patch('contact-information', [ProfileContactInformationController::class, 'update'])->name('profile.contact-information.update');

    });

    Route::resource('product-photo', ProductPhotoController::class)->only(['store','destroy']);
    Route::get("product/get-subcategories/{categoryId}", [ProductController::class, 'getSubCategories']);
    Route::get("product", [ProductController::class, 'index'])->name('product.index');
    Route::resource("product", ProductController::class);

});


require __DIR__ . '/auth.php';
