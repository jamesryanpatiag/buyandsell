const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/register.js', 'public/js')
    .js('resources/js/edit-profile.js', 'public/js')
    .js('resources/js/login-registration.js', 'public/js')
    .js('resources/js/create-product.js', 'public/js')
    .js('resources/js/edit-product.js', 'public/js')
    .sass('resources/scss/base.scss', 'public/css')
    .sass('resources/scss/auth.scss', 'public/css')
    .sass('resources/scss/dashboard.scss', 'public/css')
    .sass('resources/scss/home.scss', 'public/css')
    .sass('resources/scss/profile.scss', 'public/css')
    .sass('resources/scss/edit-profile.scss', 'public/css')
    .sass('resources/scss/login-registration.scss', 'public/css')
    .sass('resources/scss/create-product.scss', 'public/css')
    .sass('resources/scss/edit-product.scss', 'public/css')
    .postCss('resources/css/app.css', 'public/css', [
        require('postcss-import'),
        require('tailwindcss'),
        require('autoprefixer'),
    ]);

// if (mix.inProduction()) {
    mix.version();
// }
