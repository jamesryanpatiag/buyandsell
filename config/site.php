<?php
return [
    'social' => [
        'facebook'=>'https://www.facebook.com/',
        'linkedin'=>'https://www.linkedin.com/',
        'instagram'=>'https://www.instagram.com/',
        'twitter'=>'https://twitter.com/',
        'snapchat'=>'https://www.snapchat.com/',
        'support_email'=>'marketing@iconequipment.com.ph',
        'support_mobile'=>'0917-221-2340'
    ],
    'support' => [
        'email' => 'support@equipmentexchange.com',
        'phone' => ''
    ]
];
