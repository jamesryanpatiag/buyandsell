<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\BasicModel
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read mixed $select_display
 * @method static \Illuminate\Database\Eloquent\Builder|BasicModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BasicModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BasicModel query()
 */
	class BasicModel extends \Eloquent implements \OwenIt\Auditing\Contracts\Auditable {}
}

namespace App\Models{
/**
 * App\Models\Brand
 *
 * @method static selectOptions()
 * @property int $id
 * @property string $name
 * @property int $is_top
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read mixed $select_display
 * @method static \Illuminate\Database\Eloquent\Builder|Brand newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Brand newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Brand query()
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereIsTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereUpdatedAt($value)
 */
	class Brand extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Category
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read mixed $select_display
 * @method static \Illuminate\Database\Eloquent\Builder|Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereUpdatedAt($value)
 */
	class Category extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\EquipmentMileage
 *
 * @method static create(int[] $array)
 * @method static selectOptions()
 * @property string name
 * @property int $id
 * @property int $min
 * @property int $max
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read string $name
 * @property-read mixed $select_display
 * @method static \Illuminate\Database\Eloquent\Builder|EquipmentMileage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EquipmentMileage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EquipmentMileage query()
 * @method static \Illuminate\Database\Eloquent\Builder|EquipmentMileage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EquipmentMileage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EquipmentMileage whereMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EquipmentMileage whereMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EquipmentMileage whereUpdatedAt($value)
 */
	class EquipmentMileage extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\FuelType
 *
 * @method static selectOptions()
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read mixed $select_display
 * @method static \Illuminate\Database\Eloquent\Builder|FuelType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FuelType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FuelType query()
 * @method static \Illuminate\Database\Eloquent\Builder|FuelType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FuelType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FuelType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FuelType whereUpdatedAt($value)
 */
	class FuelType extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\LocationArea
 *
 * @property int id
 * @method static create(array $array)
 * @method static selectOptions()
 * @method static LocationArea findOrFail($id)
 * @property Collection|LocationRegion[] location_regions
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read mixed $select_display
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\LocationRegion[] $location_regions
 * @property-read int|null $location_regions_count
 * @method static \Illuminate\Database\Eloquent\Builder|LocationArea newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationArea newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationArea query()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationArea whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationArea whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationArea whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationArea whereUpdatedAt($value)
 */
	class LocationArea extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\LocationProvince
 *
 * @property int id
 * @method static create(array $array)
 * @method static LocationProvince findOrFail($id)
 * @property LocationRegion location_region
 * @property int $id
 * @property string $name
 * @property int $location_region_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read mixed $select_display
 * @property-read \App\Models\LocationRegion $location_region
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProvince newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProvince newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProvince query()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProvince selectOptions()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProvince whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProvince whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProvince whereLocationRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProvince whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProvince whereUpdatedAt($value)
 */
	class LocationProvince extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\LocationRegion
 *
 * @property int id
 * @method static create(array $array)
 * @method static LocationRegion findOrFail($id)
 * @property Collection|LocationProvince[] location_provinces
 * @property LocationArea location_area
 * @property int $id
 * @property string $name
 * @property int $location_area_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read mixed $select_display
 * @property-read \App\Models\LocationArea $location_area
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\LocationProvince[] $location_provinces
 * @property-read int|null $location_provinces_count
 * @method static \Illuminate\Database\Eloquent\Builder|LocationRegion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationRegion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationRegion query()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationRegion selectOptions()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationRegion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationRegion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationRegion whereLocationAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationRegion whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationRegion whereUpdatedAt($value)
 */
	class LocationRegion extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Media
 *
 * @property int id
 * @property string file_name
 * @property int $id
 * @property string $model_type
 * @property int $model_id
 * @property string|null $uuid
 * @property string $collection_name
 * @property string $name
 * @property string $file_name
 * @property string|null $mime_type
 * @property string $disk
 * @property string|null $conversions_disk
 * @property int $size
 * @property array $manipulations
 * @property array $custom_properties
 * @property array $generated_conversions
 * @property array $responsive_images
 * @property int|null $order_column
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read string $extension
 * @property-read string $human_readable_size
 * @property-read mixed $original_url
 * @property-read mixed $preview_url
 * @property-read string $type
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $model
 * @method static \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|static[] all($columns = ['*'])
 * @method static \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|static[] get($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|Media newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Media newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Media ordered()
 * @method static \Illuminate\Database\Eloquent\Builder|Media query()
 * @method static \Illuminate\Database\Eloquent\Builder|Media whereCollectionName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Media whereConversionsDisk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Media whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Media whereCustomProperties($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Media whereDisk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Media whereFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Media whereGeneratedConversions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Media whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Media whereManipulations($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Media whereMimeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Media whereModelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Media whereModelType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Media whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Media whereOrderColumn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Media whereResponsiveImages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Media whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Media whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Media whereUuid($value)
 */
	class Media extends \Eloquent implements \OwenIt\Auditing\Contracts\Auditable {}
}

namespace App\Models{
/**
 * App\Models\Member
 *
 * @property string username
 * @property string email
 * @property string mobile_number
 * @property string password
 * @property string location_province_id
 * @property string first_name
 * @property string last_name
 * @property string company_name
 * @property string bio
 * @property string profile_photo
 * @property string email_verified_status
 * @method static Member make(array $validated)
 * @method static Member forceCreate(array $validated)
 * @property Collection|Product[] products
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string|null $mobile_number
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $company_name
 * @property string|null $bio
 * @property string $profile_photo
 * @property string $password
 * @property string|null $email_verified_at
 * @property string|null $remember_token
 * @property int $location_province_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read string $email_verified_status
 * @property-read string $full_name
 * @property-read mixed $select_display
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|\App\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|Member newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Member newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Member query()
 * @method static \Illuminate\Database\Eloquent\Builder|Member whereBio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Member whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Member whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Member whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Member whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Member whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Member whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Member whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Member whereLocationProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Member whereMobileNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Member wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Member whereProfilePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Member whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Member whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Member whereUsername($value)
 */
	class Member extends \Eloquent implements \Illuminate\Contracts\Auth\Authenticatable, \Illuminate\Contracts\Auth\Access\Authorizable, \Illuminate\Contracts\Auth\CanResetPassword, \Illuminate\Contracts\Auth\MustVerifyEmail, \Spatie\MediaLibrary\HasMedia {}
}

namespace App\Models{
/**
 * App\Models\Package
 *
 * @property int id
 * @property int days_valid
 * @property string duration
 * @property string name
 * @property int photo_limit
 * @property boolean is_featured
 * @property boolean is_premium
 * @property boolean can_add_video
 * @property int $id
 * @property string $name
 * @property int $days_valid
 * @property int $photo_limit
 * @property bool $is_featured
 * @property bool $is_premium
 * @property string $price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read bool $can_add_video
 * @property-read string $duration
 * @property-read mixed $select_display
 * @method static \Illuminate\Database\Eloquent\Builder|Package newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Package newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Package query()
 * @method static \Illuminate\Database\Eloquent\Builder|Package whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Package whereDaysValid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Package whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Package whereIsFeatured($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Package whereIsPremium($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Package whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Package wherePhotoLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Package wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Package whereUpdatedAt($value)
 */
	class Package extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Product
 *
 * @property int id
 * @property int main_image_id
 * @property int $id
 * @property int $member_id
 * @property string $title
 * @property int|null $main_image_id
 * @property string $description
 * @property string $price
 * @property int $rental
 * @property string|null $rental_price_daily
 * @property string|null $rental_price_weekly
 * @property string|null $rental_price_monthly
 * @property string $model
 * @property string $brand
 * @property int $year
 * @property int $product_condition_id
 * @property int $product_status_id
 * @property int $equipment_mileage_id
 * @property string|null $serial_number
 * @property string|null $plate_number
 * @property string|null $engine_make
 * @property int|null $hours_of_use
 * @property int|null $fuel_type_id
 * @property int|null $front_tire_size
 * @property int|null $rear_tire_size
 * @property int|null $max_height
 * @property int|null $max_width
 * @property int|null $weight
 * @property string|null $youtube_url
 * @property string $contact_first_name
 * @property string $contact_last_name
 * @property string $contact_address
 * @property int $contact_location_province_id
 * @property string $contact_number
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read mixed $select_display
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|\App\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \App\Models\Member $member
 * @method static \Illuminate\Database\Eloquent\Builder|Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereBrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereContactAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereContactFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereContactLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereContactLocationProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereContactNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereEngineMake($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereEquipmentMileageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereFrontTireSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereFuelTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereHoursOfUse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereMainImageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereMaxHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereMaxWidth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product wherePlateNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereProductConditionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereProductStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereRearTireSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereRental($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereRentalPriceDaily($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereRentalPriceMonthly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereRentalPriceWeekly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereSerialNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereWeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereYoutubeUrl($value)
 */
	class Product extends \Eloquent implements \Spatie\MediaLibrary\HasMedia {}
}

namespace App\Models{
/**
 * App\Models\ProductCondition
 *
 * @method static selectOptions()
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read mixed $select_display
 * @method static \Illuminate\Database\Eloquent\Builder|ProductCondition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductCondition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductCondition query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductCondition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductCondition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductCondition whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductCondition whereUpdatedAt($value)
 */
	class ProductCondition extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ProductPackage
 *
 * @property int $id
 * @property int|null $product_id
 * @property int|null $package_id
 * @property string $expires_on
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPackage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPackage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPackage query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPackage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPackage whereExpiresOn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPackage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPackage wherePackageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPackage whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPackage whereUpdatedAt($value)
 */
	class ProductPackage extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ProductStatus
 *
 * @method static selectOptions()
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read mixed $select_display
 * @method static \Illuminate\Database\Eloquent\Builder|ProductStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductStatus whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductStatus whereUpdatedAt($value)
 */
	class ProductStatus extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Subcategory
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read mixed $select_display
 * @method static \Illuminate\Database\Eloquent\Builder|Subcategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Subcategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Subcategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|Subcategory whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subcategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subcategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subcategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subcategory whereUpdatedAt($value)
 */
	class Subcategory extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\UserAdmin
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read mixed $select_display
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Illuminate\Database\Eloquent\Builder|UserAdmin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAdmin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAdmin query()
 */
	class UserAdmin extends \Eloquent implements \Illuminate\Contracts\Auth\Authenticatable, \Illuminate\Contracts\Auth\Access\Authorizable, \Illuminate\Contracts\Auth\CanResetPassword {}
}

