<?php

namespace Tests\Feature;

use App\Helpers\ListHelper;
use App\Models\LocationProvince;
use App\Providers\RouteServiceProvider;
use App\View\Components\Form\BasicFormComponent;
use Arr;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
    use RefreshDatabase;

    public function test_registration_screen_can_be_rendered()
    {
        BasicFormComponent::resetFormElementsList();
        $response = $this->get('/register');

        $response->assertStatus(200);
    }

    public function test_new_users_can_register()
    {
        $response = $this->post('/register', [
            'username' => 'testuser',
            'email' => 'test@example.com',
            'password' => 'password',
            'password_confirmation' => 'password',
            'location_province_id' => LocationProvince::inRandomOrder()->first()->id,
        ]);

        $this->assertAuthenticated();
        $response->assertRedirect(RouteServiceProvider::HOME);
    }
}
