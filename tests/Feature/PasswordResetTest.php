<?php /** @noinspection PhpIllegalPsrClassPathInspection */

namespace Tests\Feature;

use App\Mail\MemberPasswordResetEmail;
use App\Models\Member;
use App\View\Components\Form\BasicFormComponent;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mail;
use Tests\TestCase;

class PasswordResetTest extends TestCase
{
    use RefreshDatabase;

    public function test_reset_password_link_screen_can_be_rendered()
    {
        $response = $this->get('/forgot-password');

        $response->assertStatus(200);
    }

    /**
     * @throws BindingResolutionException
     */
    public function test_reset_password_link_can_be_requested()
    {
        Mail::fake();
        /** @var Member $user */
        $user = Member::factory()->create();

        $this->post('/forgot-password', ['email' => $user->email]);


        Mail::assertSent(MemberPasswordResetEmail::class);

    }

    public function test_reset_password_screen_can_be_rendered()
    {
        BasicFormComponent::resetFormElementsList();
        Mail::fake();

        /** @var Member $user */
        $user = Member::factory()->create();

        $this->post('/forgot-password', ['email' => $user->email]);


        Mail::assertSent(MemberPasswordResetEmail::class, function ($mail) use($user) {
            $response = $this->get('/reset-password/'.$mail->token);

            $response->assertStatus(200);
            $this->assertTrue($mail->hasTo($user->email));

            return true;
        });
    }

    public function test_password_can_be_reset_with_valid_token()
    {
        Mail::fake();

        /** @var Member $user */
        $user = Member::factory()->create();

        $this->post('/forgot-password', ['email' => $user->email]);

        Mail::assertSent(MemberPasswordResetEmail::class, function ($mail) use ($user) {
            $response = $this->post('/reset-password', [
                'token' => $mail->token,
                'email' => $user->email,
                'password' => 'password',
                'password_confirmation' => 'password',
            ]);

            $response->assertSessionHasNoErrors();
            $this->assertTrue($mail->hasTo($user->email));

            return true;
        });
    }
}
