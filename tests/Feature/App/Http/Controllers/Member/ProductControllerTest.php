<?php /** @noinspection PhpIllegalPsrClassPathInspection */

namespace Tests\Feature\App\Http\Controllers\Member;


use App\Models\Media;
use App\Models\Member;
use App\Models\Product;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class ProductControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function testStore()
    {
        $images = [
            $this->faker->text(100) => UploadedFile::fake()->image("img1.png", 100),
            $this->faker->text(100) => UploadedFile::fake()->image("img2.png", 100),
            $this->faker->text(100) => UploadedFile::fake()->image("img3.png", 100),
        ];
        $data = [
            'file' => array_values($images),
        ];
        $initial_count = Product::count();


        $response = $this->actingAs(Member::factory()->create())->post(route('product.store'), $data);


        //$response->assertRedirect();
        $response->assertJson(['success' => true]);
        $this->assertEquals($initial_count + 1, Product::count());
        $product = Product::latest('id')->firstOrFail();
        $photos = $product->getPhotos();
        $this->assertEquals(3, $photos->count());
        foreach($photos as $photo) {
            /** @var Media $photo */
            $this->assertTrue(key_exists($photo->getDescription(), $images));
            $this->assertEquals($images[$photo->getDescription()]->name, $photo->file_name);
        }



    }
}
