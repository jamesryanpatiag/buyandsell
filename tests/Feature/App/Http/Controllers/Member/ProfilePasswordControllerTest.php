<?php /** @noinspection PhpIllegalPsrClassPathInspection */

namespace Tests\Feature\App\Http\Controllers\Member;


use App\Models\Member;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProfilePasswordControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    public function testEdit()
    {
        $response = $this->actingAs(Member::factory()->create())->get(route('profile.password.edit'));
        $response->assertOk();
    }

    public function testUpdate()
    {
        $new_password = $this->faker->password(8);
        $old_password = 'password123456';
        /** @var Member $member */
        $member = Member::factory()->create([
            'password' => $old_password,
        ]);

        $response = $this->actingAs($member)->patch(route('profile.password.update'), [
            'old_password' => $old_password,
            'new_password' => $new_password,
            'new_password_confirmation' => $new_password,
        ]);

        $response->assertRedirect(route('profile.password.edit'));
        $response->assertSessionHasNoErrors();

        $member->refresh();

        $response = $this->post(route('login'), [
            'username' => $member->username,
            'password' => $new_password,
        ]);

        $this->assertAuthenticated();
    }
}
