<?php /** @noinspection PhpIllegalPsrClassPathInspection */

namespace Tests\Feature\App\Http\Controllers\Member;


use App\Helpers\ListHelper;
use App\Models\LocationProvince;
use App\Models\Member;
use App\View\Components\Form\BasicFormComponent;
use Arr;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProfileControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    public function testEdit()
    {
        $response = $this->actingAs(Member::factory()->create())->get(route('profile.edit'));
        $response->assertOk();
    }

    public function testUpdate()
    {
        /** @var Member $member */
        $member = Member::factory()->create();

        $data = [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'company_name' => $this->faker->company,
            'bio' => $this->faker->text(255),
            'location_province_id' => LocationProvince::inRandomOrder()->first()->id,
        ];

        $response = $this->actingAs($member)
            ->patch(route('profile.update'), $data);

        $response->assertSessionHasNoErrors();
        $response->assertRedirect();

        $member->refresh();
        $this->assertEquals($data['first_name'], $member->first_name);
        $this->assertEquals($data['last_name'], $member->last_name);
        $this->assertEquals($data['bio'], $member->bio);
        $this->assertEquals($data['company_name'], $member->company_name);
        $this->assertEquals($data['location_province_id'], $member->location_province_id);
    }

    public function testIndex()
    {
        $response = $this->actingAs(Member::factory()->create())->get(route('profile.index'));
        $response->assertOk();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        BasicFormComponent::resetFormElementsList();

    }
}
