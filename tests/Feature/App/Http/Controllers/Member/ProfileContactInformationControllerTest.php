<?php /** @noinspection PhpIllegalPsrClassPathInspection */

namespace Tests\Feature\App\Http\Controllers\Member;


use App\Models\Member;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProfileContactInformationControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase, DatabaseMigrations;

    public function testEdit()
    {
        $response = $this->actingAs(Member::factory()->create())->get(route('profile.contact-information.edit'));
        $response->assertOk();
    }

    public function testUpdate()
    {
        $mobile_number = $this->faker->unique->phoneNumber;
        $email = $this->faker->unique->email;
        /** @var Member $member */
        $member = Member::factory()->create();

        $this->assertNotEquals($mobile_number, $member->mobile_number);
        $this->assertNotEquals($email, $member->email);

        $response = $this->actingAs($member)->patch(route('profile.contact-information.update'), [
            'mobile_number' => $mobile_number,
            'email' => $email,
        ]);

        $response->assertRedirect(route('profile.contact-information.edit'));
        $response->assertSessionHasNoErrors();


        $member->refresh();

        $this->assertEquals($mobile_number, $member->mobile_number);
        $this->assertEquals($email, $member->email);

    }
}
